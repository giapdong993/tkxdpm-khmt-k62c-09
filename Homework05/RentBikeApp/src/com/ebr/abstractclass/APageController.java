package com.ebr.abstractclass;

/**
 * Abstract class for all Controller page.
 * 
 * @author Giap Dong
 * @since 1.0
 */
public abstract class APageController<T> {

	/**
	 * Page instance that this controller will control.
	 */
	protected T page;

	/**
	 * Show page that this controller have
	 *
	 * @since 1.0
	 */
	public abstract void showPage();

	/**
	 * Disappear page that this controller have
	 *
	 * @since 1.0
	 */
	public abstract void closePage();

	/**
	 * Add page that this controller will control it
	 *
	 * @since 1.0
	 */
	public abstract void addPage(T t);
}
