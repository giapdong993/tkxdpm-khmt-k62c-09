package com.ebr.api;

import java.util.ArrayList;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.bean.CreditCard;

public class PaymentApi implements IPaymentApi {
	private static IPaymentApi singleton = new PaymentApi();
	private Client client = ClientBuilder.newClient();

	private PaymentApi() {

	}

	public static IPaymentApi singleton() {
		return singleton;
	}

	@Override
	public ArrayList<CreditCard> intensionPayment(CreditCard card) {
		WebTarget webTarget = client.target(PATH).path("Payment").path("deduction");
		webTarget = webTarget.queryParam("cardNumber", card.getCardNumber());
		webTarget = webTarget.queryParam("dateExpired", card.getDateExpired());
		webTarget = webTarget.queryParam("secretNumber", card.getSecretNumber());
		webTarget = webTarget.queryParam("totalMoney", card.getTotalMoney());

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<CreditCard> res = response.readEntity(new GenericType<ArrayList<CreditCard>>(){});
//		System.out.println(res);
		return res;
	}

	@Override
	public ArrayList<CreditCard> deductionPayment(CreditCard card) {
		WebTarget webTarget = client.target(PATH).path("Payment").path("intension");
		webTarget = webTarget.queryParam("cardNumber", card.getCardNumber());
		webTarget = webTarget.queryParam("dateExpired", card.getDateExpired());
		webTarget = webTarget.queryParam("secretNumber", card.getSecretNumber());
		webTarget = webTarget.queryParam("totalMoney", card.getTotalMoney());

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<CreditCard> res = response.readEntity(new GenericType<ArrayList<CreditCard>>(){});
//		System.out.println(res);
		return res;
	}

}
