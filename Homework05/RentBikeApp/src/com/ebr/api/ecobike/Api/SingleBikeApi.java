package com.ebr.api.ecobike.Api;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.api.ecobike.interfaceApi.IEcoBikeApi;
import com.ebr.api.ecobike.interfaceApi.ISingleBikeApi;
import com.ebr.bean.SingleBike;

public class SingleBikeApi implements ISingleBikeApi{
	private Client client;
	public static final String PATH = "http://localhost:8081/";
	
	
	
	public SingleBikeApi() {
		client = ClientBuilder.newClient();
	}
	private static ISingleBikeApi singleton;
	
	public static synchronized ISingleBikeApi getSingleton() {
		if (singleton == null)
		singleton = new SingleBikeApi();
		return singleton;
		}
	@Override
	public ArrayList<SingleBike> getSingleBikes(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("SingleBike");
		
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<SingleBike> res = response.readEntity(new GenericType<ArrayList<SingleBike>>() {});
		System.out.println(res);
		return res;
	}

	
	@Override
	public SingleBike updateSingleBike(SingleBike singlebike) {
		WebTarget webTarget = client.target(PATH).path("SingleBike").path("update");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(singlebike, MediaType.APPLICATION_JSON));
		
		SingleBike res = response.readEntity(SingleBike.class);
		System.out.println(response);
		return res;
	}
	
	
	@Override
	public ArrayList<SingleBike> addSingleBike(SingleBike singlebike) {
		WebTarget webTarget = client.target(PATH).path("SingleBike").path(singlebike.getId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(singlebike, MediaType.APPLICATION_JSON));
		if (response.getStatus() !=  400) {
			ArrayList<SingleBike> res = response.readEntity(new GenericType<ArrayList<SingleBike>>() {});
			System.out.println(response);
			return res;
		}else {
		
		System.out.println(response);
		return null;
		}
	}
	@Override
	public ArrayList<SingleBike> deleteSingleBike(SingleBike singlebike) {
		WebTarget webTarget = client.target(PATH).path("SingleBike").path("delete");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(singlebike, MediaType.APPLICATION_JSON));
		if (response.getStatus() ==  200) {
			ArrayList<SingleBike> res = response.readEntity(new GenericType<ArrayList<SingleBike>>() {});
			System.out.println(response);
			return res;
		}else {
			
		System.out.println(response);
		return null;
		}
	}
}
