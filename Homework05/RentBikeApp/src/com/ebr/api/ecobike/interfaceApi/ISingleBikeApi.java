package com.ebr.api.ecobike.interfaceApi;

import java.util.ArrayList;
import java.util.Map;

import com.ebr.bean.SingleBike;

public interface ISingleBikeApi {
	public ArrayList<SingleBike> getSingleBikes(Map<String, String> queryParams);
	public SingleBike updateSingleBike(SingleBike singlebike);
	public ArrayList<SingleBike> addSingleBike(SingleBike singlebike);
	public ArrayList<SingleBike> deleteSingleBike(SingleBike singlebike);
}
