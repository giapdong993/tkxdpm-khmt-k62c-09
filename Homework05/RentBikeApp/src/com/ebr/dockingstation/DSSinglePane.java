package com.ebr.dockingstation;

import javax.swing.JLabel;

import com.ebr.bean.DockingStation;

import abstractdata.ADataSinglePane;

@SuppressWarnings("serial")
public class DSSinglePane extends ADataSinglePane<DockingStation>{
	private JLabel labelStationId;
	private JLabel labelStationName;
	private JLabel labelStationAddress;
	private JLabel labelNumberOfAllDocks;
	private JLabel labelNumberOfBikes;
	private JLabel labelNumberOfEBikes;
	private JLabel labelNumberOfTwinBikes;	
	private JLabel labelIdSingleBike;
	
	public DSSinglePane() {
		super();
	}
		
	
	public DSSinglePane(DockingStation ds) {
		this();
		this.t = ds;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelStationId = new JLabel();
		add(labelStationId, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelStationName = new JLabel();
		add(labelStationName, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelStationAddress = new JLabel();
		add(labelStationAddress, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelIdSingleBike = new JLabel();
		add(labelIdSingleBike, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfAllDocks = new JLabel();
		add(labelNumberOfAllDocks, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfBikes = new JLabel();
		add(labelNumberOfBikes, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfEBikes = new JLabel();
		add(labelNumberOfEBikes, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfTwinBikes = new JLabel();
		add(labelNumberOfTwinBikes, c);
	}
	
	
	@Override
	public void displayData() {
		labelStationId.setText("Station ID: " + t.getStationId());
		labelStationName.setText("Station Name: " + t.getStationName());
		labelStationAddress.setText("Station Address: " + t.getStationAddress());
		labelIdSingleBike.setText("List Bike Id: " + t.getId());
		labelNumberOfAllDocks.setText("Number Of All Docks: " + t.getNumberOfAllDocks());
		labelNumberOfBikes.setText("Number Of Bikes: " + t.getNumberOfBikes());
		labelNumberOfEBikes.setText("Number Of EBikes: " + t.getNumberOfEBikes());
		labelNumberOfTwinBikes.setText("Number Of TwinBikes: " + t.getNumberOfTwinBikes());
		
	}


	@Override
	public void removeData() {
		// TODO Auto-generated method stub
		
	}
}
