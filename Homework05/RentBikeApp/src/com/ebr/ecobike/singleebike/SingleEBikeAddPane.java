package com.ebr.ecobike.singleebike;

import java.util.Map;

import com.ebr.ecobike.gui.EBikeAddPane;

@SuppressWarnings("serial")
public class SingleEBikeAddPane extends EBikeAddPane{
	public SingleEBikeAddPane() {
		super();
	} 
	
	@Override
	public void buildControls() {
		super.buildControls();
		
	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		return res;
	}
}
