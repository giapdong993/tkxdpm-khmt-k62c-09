package com.ebr.ecobike.gui;



import javax.swing.JLabel;

import com.ebr.bean.EcoBike;

import abstractdata.ADataSinglePane;

@SuppressWarnings("serial")
public class EcoBikeSinglePane extends ADataSinglePane<EcoBike>{
	private JLabel labelId;
	private JLabel labelName;
	private JLabel labelType;
	private JLabel labelWeight;
	private JLabel licensePlate;
	private JLabel manuafacturingDate;
	private JLabel producer;
	private JLabel labelCost;
	private JLabel idDockingStation;
	
	public EcoBikeSinglePane() {
		super();
	}
		
	
	public EcoBikeSinglePane(EcoBike ecobike) {
		this();
		this.t = ecobike;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelId = new JLabel();
		add(labelId, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelName = new JLabel();
		add(labelName, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelType = new JLabel();
		add(labelType, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelWeight = new JLabel();
		add(labelWeight, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		licensePlate = new JLabel();
		add(licensePlate, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		manuafacturingDate = new JLabel();
		add(manuafacturingDate, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		producer = new JLabel();
		add(producer, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelCost = new JLabel();
		add(labelCost, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		idDockingStation = new JLabel();
		add(idDockingStation, c);
	}
	
	
	@Override
	public void displayData() {
		
			labelId.setText("Id: " + t.getId());
			labelName.setText("Name: " + t.getName());
			labelType.setText("Type: " + t.getType());
			labelWeight.setText("Weight: " + t.getWeight());
			licensePlate.setText("License Plate: " + t.getLicensePlate());
			manuafacturingDate.setText("Manuafacturing Date: " + t.getManuafacturingDate());
			producer.setText("Producer: " + t.getProducer());
			labelCost.setText("Cost: " + t.getCost() + "");
			idDockingStation.setText("Id Docking Station" + t.getIdDockingStation() + "");
		
	}


	@Override
	public void removeData() {
		labelId.setVisible(false);
		labelName.setVisible(false);
		labelType.setVisible(false);
		labelWeight.setVisible(false);
		licensePlate.setVisible(false);
		manuafacturingDate.setVisible(false);
		producer.setVisible(false);
		labelCost.setVisible(false);
		idDockingStation.setVisible(false);
	}
}

