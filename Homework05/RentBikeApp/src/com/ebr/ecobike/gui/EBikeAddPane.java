package com.ebr.ecobike.gui;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class EBikeAddPane extends EcoBikeAddPane{
	private JTextField batteryPercentageField;
	private JTextField loadCyclesField;
	private JTextField usageTimeField;
	public EBikeAddPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		super.buildControls();
		
		
		JLabel batteryPercentageLabel = new JLabel("Battery Percentage");
		batteryPercentageField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(batteryPercentageLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(batteryPercentageField, c);
		
		JLabel loadCyclesLabel = new JLabel("LoadCycles");
		loadCyclesField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(loadCyclesLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(loadCyclesField, c);
		
		JLabel usageTimeLabel = new JLabel("Usage Time");
		usageTimeField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(usageTimeLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(usageTimeField, c);
	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		if (!batteryPercentageField.getText().trim().equals("")) {
			res.put("batteryPercentage", batteryPercentageField.getText().trim());
		}
		if (!loadCyclesField.getText().trim().equals("")) {
			res.put("loadCycles", loadCyclesField.getText().trim());
		}
		if (!usageTimeField.getText().trim().equals("")) {
			res.put("usageTime", batteryPercentageField.getText().trim());
		}
		return res;
	}
}
