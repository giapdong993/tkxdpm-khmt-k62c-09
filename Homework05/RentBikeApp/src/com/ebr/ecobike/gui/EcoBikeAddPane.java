package com.ebr.ecobike.gui;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import abstractdata.ADataAddPane;
import abstractdata.ADataSearchPane;

@SuppressWarnings("serial")
public class EcoBikeAddPane extends ADataAddPane {
	private JTextField idField;
	private JTextField nameField;
	private JTextField typeField;
	private JTextField weightField;
	private JTextField licensePlateField;
	private JTextField manuafacturingDateField;
	private JTextField producerField;
	private JTextField costField;
	private JTextField idDockingStationField;
	public EcoBikeAddPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		JLabel idLabel = new JLabel("Id");
		idField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(idLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(idField, c);
		
		
		JLabel nameLabel = new JLabel("Name");
		nameField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(nameLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(nameField, c);
		
		JLabel typeLabel = new JLabel("Type");
		typeField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(typeLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(typeField, c);
		
		JLabel weightLabel = new JLabel("Weight");
		weightField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(weightLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(weightField, c);
		
		JLabel licensePlateLabel = new JLabel("License Plate");
		licensePlateField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(licensePlateLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(licensePlateField, c);
		
		JLabel manuafacturingDateLabel = new JLabel("Manuafacturing Date");
		manuafacturingDateField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(manuafacturingDateLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(manuafacturingDateField, c);
		
		JLabel producerLabel = new JLabel("Producer");
		producerField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(producerLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(producerField, c);
		
		JLabel costLabel = new JLabel("Cost");
		costField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(costLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(costField, c);
		
		JLabel idDockingStationLabel = new JLabel("idDockingStation");
		idDockingStationField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(idDockingStationLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(idDockingStationField, c);
		
		
	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		if (!idField.getText().trim().equals("")) {
			res.put("id", idField.getText().trim());
		}
		if (!nameField.getText().trim().equals("")) {
			res.put("name", nameField.getText().trim());
		}
		if (!typeField.getText().trim().equals("")) {
			res.put("type", typeField.getText().trim());
		}
		if (!weightField.getText().trim().equals("")) {
			res.put("weight", weightField.getText().trim());
		}
		if (!licensePlateField.getText().trim().equals("")) {
			res.put("licensePlate", licensePlateField.getText().trim());
		}
		if (!manuafacturingDateField.getText().trim().equals("")) {
			res.put("manuafacturingDate", manuafacturingDateField.getText().trim());
		}
		if (!producerField.getText().trim().equals("")) {
			res.put("producer", producerField.getText().trim());
		}
		if (!costField.getText().trim().equals("")) {
			res.put("cost", costField.getText().trim());
		}
		if (!idDockingStationField.getText().trim().equals("")) {
			res.put("idDockingStation", idDockingStationField.getText().trim());
		}
		
		return res;
	}
}
