package com.ebr.ecobike.gui;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ebr.bean.EcoBike;

import abstractdata.ADataEditDialog;
import abstractdata.IDataManageController;




@SuppressWarnings("serial")
public class EcoBikeEditDialog extends ADataEditDialog<EcoBike>{
	
	private JTextField idField;
	private JTextField nameField;
	private JTextField typeField;
	private JTextField weightField;
	private JTextField licensePlateField;
	
	private JTextField producerField;
	private JTextField costField;
	private JTextField idDockingStationField;
	public EcoBikeEditDialog(EcoBike ecobike, IDataManageController<EcoBike> controller) {
		super(ecobike, controller);
	}

	@Override
	public void buildControls() {
		int row = getLastRowIndex();
		JLabel idLabel = new JLabel("Id");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(idLabel, c);
		idField = new JTextField(15);
		idField.setText(t.getId());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(idField, c);
		
		
		row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Name");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);
		nameField.setText(t.getName());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);
		
		
		row = getLastRowIndex();
		JLabel typeLabel = new JLabel("Type");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(typeLabel, c);
		typeField = new JTextField(15);
		typeField.setText(t.getType() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(typeField, c);
		
		row = getLastRowIndex();
		JLabel weightLabel = new JLabel("Weight");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(weightLabel, c);
		weightField = new JTextField(15);
		weightField.setText(t.getWeight() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(weightField, c);
		
		row = getLastRowIndex();
		JLabel licensePlateLabel = new JLabel("License Plate");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(licensePlateLabel, c);
		licensePlateField = new JTextField(15);
		licensePlateField.setText(t.getLicensePlate() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(licensePlateField, c);
		
		
		
		row = getLastRowIndex();
		JLabel producerLabel = new JLabel("Producer");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(producerLabel, c);
		producerField = new JTextField(15);
		producerField.setText(t.getProducer() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(producerField, c);
		
		row = getLastRowIndex();
		JLabel costLabel = new JLabel("Cost");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(costLabel, c);
		costField = new JTextField(15);
		costField.setText(t.getCost() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(costField, c);
		
		row = getLastRowIndex();
		JLabel idDockingStationLabel = new JLabel("Id Docking Station");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(idDockingStationLabel, c);
		idDockingStationField = new JTextField(15);
		idDockingStationField.setText(t.getIdDockingStation() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(idDockingStationField, c);
	}
	
	@Override
	public EcoBike getNewData() {
		t.setId(idField.getText());
		t.setName(nameField.getText());
		t.setType(typeField.getText());
		t.setWeight(Integer.parseInt(weightField.getText()));
		t.setLicensePlate(licensePlateField.getText());
		
		t.setProducer(producerField.getText());
		
		t.setCost(Integer.parseInt(costField.getText()));
		t.setIdDockingStation(idDockingStationField.getText());
		return t;
	}
}
