package com.ebr.ecobike.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ebr.api.ecobike.Api.EcoBikeApi;
import com.ebr.api.ecobike.Api.SingleBikeApi;
import com.ebr.bean.EcoBike;
import com.ebr.bean.SingleBike;
import com.ebr.ecobike.gui.AdminEcoBikeListPane;
import com.oms.bean.Book;
import com.oms.bean.Media;


import abstractdata.ADataListPane;
import abstractdata.ADataPageController;


public abstract class AdminEcoBikePageController extends ADataPageController<EcoBike> {
	
	
	
	
	
	public AdminEcoBikePageController() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public ADataListPane<EcoBike> createListPane() {
		return new AdminEcoBikeListPane(this);
	}
	
	 public List<? extends EcoBike> delete(String id, String name, String type, int weight, String licensePlate, Date manuafacturingDate, String producer, int cost, String idDockingStation){
		 SingleBike  singlebike = new SingleBike(id,name,type,weight,licensePlate,manuafacturingDate,producer,cost, idDockingStation);
		 return null;
	 }
	 public EcoBike updateEcoBike(EcoBike ecobike) {
			return new SingleBikeApi().updateSingleBike((SingleBike) ecobike);
		}
}

