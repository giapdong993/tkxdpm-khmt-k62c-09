package com.ebr.ecobike.twinbike;

import com.ebr.bean.EcoBike;
import com.ebr.ecobike.gui.BikeSinglePane;

@SuppressWarnings("serial")
public class TwinBikeSinglePane extends BikeSinglePane {
	
	public TwinBikeSinglePane() {
		super();
	}
	
	public TwinBikeSinglePane(EcoBike ecobike) {
		this();
		this.t = ecobike;

		displayData();
	}
}
