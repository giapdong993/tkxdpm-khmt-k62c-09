package com.chargeMethod.controller;

import com.chargeMethod.model.ChargeMethod;
import com.chargeMethod.view.ChangeChargeMethodPage;

public class InfoChargeMethodPageController {
	
	public void getChangeChargeMethodPage() {
		new ChangeChargeMethodPage(new ChangeChargeMethodPageController());
	}
	
	public ChargeMethod getInfoChargeMethod() {
		return new ChargeMethod(20000, 10000, 2000);
	}
	
	public void setChargeMethodInView(ChargeMethod chargeMethod) {
		
	}
	
}
