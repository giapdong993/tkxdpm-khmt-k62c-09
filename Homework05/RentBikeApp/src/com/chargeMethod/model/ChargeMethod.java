package com.chargeMethod.model;

import java.io.Serializable;

public class ChargeMethod implements Serializable{

	private int fee24h;
	
	private int fee_decrease_before12h_1h;
	
	private int fee_increase_after24h_15mi;

	public ChargeMethod() {
		super();
	}

	public ChargeMethod(int fee24h, int fee_decrease_before12h_1h, int fee_increase_after24h_15mi) {
		super();
		this.fee24h = fee24h;
		this.fee_decrease_before12h_1h = fee_decrease_before12h_1h;
		this.fee_increase_after24h_15mi = fee_increase_after24h_15mi;
	}

	public int getFee24h() {
		return fee24h;
	}

	public void setFee24h(int fee24h) {
		this.fee24h = fee24h;
	}

	public int getFee_decrease_before12h_1h() {
		return fee_decrease_before12h_1h;
	}

	public void setFee_decrease_before12h_1h(int fee_decrease_before12h_1h) {
		this.fee_decrease_before12h_1h = fee_decrease_before12h_1h;
	}

	public int getFee_increase_after24h_15mi() {
		return fee_increase_after24h_15mi;
	}

	public void setFee_increase_after24h_15mi(int fee_increase_after24h_15mi) {
		this.fee_increase_after24h_15mi = fee_increase_after24h_15mi;
	}
	
	public boolean changeChargeMethod(ChargeMethod chargeMethod) {
		
		return true;
	}
}
