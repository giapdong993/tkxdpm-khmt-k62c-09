package com.chargeMethod.view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.chargeMethod.controller.InfoChargeMethodPageController;
import com.chargeMethod.controller.SerializeFileFactory;
import com.chargeMethod.model.ChargeMethod;

public class InfoChargeMethodPage extends JFrame{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;
	
	public InfoChargeMethodPage(InfoChargeMethodPageController controller) {
		new JFrame("Cách tính phí thuê xe");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		JPanel panel = new JPanel();
		add(panel);
		panel.setLayout(null);
		JLabel jLabel = new JLabel("Thông tin giá thuê xe"); 
		jLabel.setBounds(30, 30, 700, 40);
		jLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
		panel.add(jLabel);

		Object data = SerializeFileFactory.readFile("info_charge_method.dat");
		if (data == null) {
			boolean kq = SerializeFileFactory.saveFile(new ChargeMethod(22222, 11111, 2222), "info_charge_method.dat");
			data = SerializeFileFactory.readFile("info_charge_method.dat");
		}
		ChargeMethod chargeMethod = (ChargeMethod) data;	
		
		JLabel price12h_24h_label = new JLabel("Giá thuê nếu thuê hơn 12h và dưới 24h:"); 
		price12h_24h_label.setBounds(30, 110, 700, 30);
		price12h_24h_label.setFont(new Font("Helvetica", Font.PLAIN, 20));
		panel.add(price12h_24h_label);
		JLabel price12h_24h = new JLabel(chargeMethod.getFee24h()+" VNĐ"); 
		price12h_24h.setBounds(30, 140, 700, 30);
		price12h_24h.setFont(new Font("Helvetica", Font.ITALIC, 18));
		panel.add(price12h_24h);

		JLabel price_before_12h_label = new JLabel("Giá tiền hoàn lại mỗi tiếng trả sớm (nếu trả xe trước 12h):");  
		price_before_12h_label.setBounds(30, 190, 700, 30);
		price_before_12h_label.setFont(new Font("Helvetica", Font.PLAIN, 20));
		panel.add(price_before_12h_label);
		JLabel price_before_12h = new JLabel(chargeMethod.getFee_decrease_before12h_1h() + " VNĐ"); 
		price_before_12h.setBounds(30, 220, 700, 30);
		price_before_12h.setFont(new Font("Helvetica", Font.ITALIC, 18));
		panel.add(price_before_12h);

		JLabel price_after_24h_label = new JLabel("Giá phải trả thêm mỗi 15 phút trả muộn (nếu trả xe muộn hơn 24h):");  
		price_after_24h_label.setBounds(30, 270, 700, 30);
		price_after_24h_label.setFont(new Font("Helvetica", Font.PLAIN, 20));
		panel.add(price_after_24h_label);
		JLabel price_after_24h = new JLabel(chargeMethod.getFee_increase_after24h_15mi() + " VNĐ"); 
		price_after_24h.setBounds(30, 300, 700, 30);
		price_after_24h.setFont(new Font("Helvetica", Font.ITALIC, 18));
		panel.add(price_after_24h);
		
		JButton changeButton = new JButton("THAY ĐỔI CÁCH TÍNH");
		changeButton.setBounds(30, 360, 300, 30);
		changeButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.getChangeChargeMethodPage();
				dispose();
			}
		});
	    panel.add(changeButton);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new InfoChargeMethodPage(new InfoChargeMethodPageController()).setVisible(true);;
			}
		});
	}
}
