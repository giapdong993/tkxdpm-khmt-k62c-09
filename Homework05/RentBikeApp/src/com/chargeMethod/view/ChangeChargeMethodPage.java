package com.chargeMethod.view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.chargeMethod.controller.ChangeChargeMethodPageController;
import com.chargeMethod.controller.InfoChargeMethodPageController;
import com.chargeMethod.controller.SerializeFileFactory;
import com.chargeMethod.model.ChargeMethod;
public class ChangeChargeMethodPage extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6703325929476848363L;
	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;
	

	public ChangeChargeMethodPage(ChangeChargeMethodPageController controller){
		new JFrame("Thay đổi cách tính phí thuê xe");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		JPanel panel = new JPanel();
		add(panel);
		panel.setLayout(null);
		JLabel jLabel = new JLabel("Thay đổi cách tính giá thuê xe"); 
		jLabel.setBounds(30, 30, 700, 40);
		jLabel.setFont(new Font("Helvetica", Font.BOLD, 36));
		panel.add(jLabel);
		


		Object data = SerializeFileFactory.readFile("info_charge_method.dat");
		ChargeMethod chargeMethod = (ChargeMethod) data;
		
		JLabel price12h_24h_label = new JLabel("Giá thuê nếu thuê hơn 12h và dưới 24h:"); 
		price12h_24h_label.setBounds(30, 110, 700, 30);
		price12h_24h_label.setFont(new Font("Helvetica", Font.PLAIN, 20));
		panel.add(price12h_24h_label);
		JTextField price12h_24h = new JTextField(chargeMethod.getFee24h()+""); 
		price12h_24h.setBounds(30, 140, 700, 30);
		price12h_24h.setFont(new Font("Helvetica", Font.ITALIC, 18));
		panel.add(price12h_24h);

		JLabel price_before_12h_label = new JLabel("Giá tiền hoàn lại mỗi tiếng trả sớm (nếu trả xe trước 12h):");  
		price_before_12h_label.setBounds(30, 190, 700, 30);
		price_before_12h_label.setFont(new Font("Helvetica", Font.PLAIN, 20));
		panel.add(price_before_12h_label);
		JTextField price_before_12h = new JTextField(chargeMethod.getFee_decrease_before12h_1h() + ""); 
		price_before_12h.setBounds(30, 220, 700, 30);
		price_before_12h.setFont(new Font("Helvetica", Font.ITALIC, 18));
		panel.add(price_before_12h);

		JLabel price_after_24h_label = new JLabel("Giá phải trả thêm mỗi 15 phút trả muộn (nếu trả xe muộn hơn 24h):");  
		price_after_24h_label.setBounds(30, 270, 700, 30);
		price_after_24h_label.setFont(new Font("Helvetica", Font.PLAIN, 20));
		panel.add(price_after_24h_label);
		JTextField price_after_24h = new JTextField(chargeMethod.getFee_increase_after24h_15mi()+ ""); 
		price_after_24h.setBounds(30, 300, 700, 30);
		price_after_24h.setFont(new Font("Helvetica", Font.ITALIC, 18));
		panel.add(price_after_24h);
		
		JButton changeButton = new JButton("OK");
		changeButton.setBounds(30, 360, 100, 30);
		changeButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int value1, value2, value3;
				try {
					value1 = Integer.parseInt(price12h_24h.getText().toString());
					value2 = Integer.parseInt(price_before_12h.getText().toString());
					value3 = Integer.parseInt(price_after_24h.getText().toString());
					if(value1 < 0 || value2 <0 || value3 < 0) {
						value1 = 0; value2 = 0; value3 = 0;
					}
				}catch (Exception e1) {
					e1.printStackTrace();
					value1 = 0; value2 = 0; value3 = 0;
				}
				controller.changeChargeMethod(value1, value2, value3);
				new InfoChargeMethodPage(new InfoChargeMethodPageController());
				dispose();
			}
		});
	    panel.add(changeButton);
		JButton cancelButton = new JButton("HỦY");
		cancelButton.setBounds(150, 360, 100, 30);
		cancelButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new InfoChargeMethodPage(new InfoChargeMethodPageController());
				dispose();
			}
		});
	    panel.add(cancelButton);
		

		
		setVisible(true);
	
	}
}
