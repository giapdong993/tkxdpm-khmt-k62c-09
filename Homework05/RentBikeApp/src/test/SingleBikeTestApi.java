package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.ebr.api.DockingStationApi;
import com.ebr.api.ecobike.Api.SingleBikeApi;
import com.ebr.bean.DockingStation;
import com.ebr.bean.SingleBike;

public class SingleBikeTestApi {
	private SingleBikeApi testApi = new SingleBikeApi();
	private DockingStationApi testDS = new DockingStationApi();
	@Test
	public void testGetSingleBikes() {
		ArrayList<SingleBike> list= testApi.getSingleBikes(null);
		assertEquals("Error in getBooks API!", list.size(), 4);
	}
	@Test(timeout = 1000)
	public void testResponse() {
		testApi.getSingleBikes(null);
	}
	@Test
	public void testAddSingleBike() {
		ArrayList<SingleBike> list = testApi.getSingleBikes(null);
		ArrayList<DockingStation> listDockingStation = testDS.getDockingStations(null);
		assertTrue("No data", list.size() > 0);
		String date = "12/07/2020";
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date dateFormat = null;

        try {

            dateFormat = formatter.parse(date);
            
        } catch (ParseException e) {
            e.printStackTrace();
        }
		SingleBike singlebike = new SingleBike("SB043","xe dap","singlebike", 50,"qwr", dateFormat,"Yamaha", 0, "DS1002");
		
		
		
		ArrayList<SingleBike> afterBike  = testApi.addSingleBike(singlebike);
		
		assertTrue("Lenght array is true", afterBike.size() - 1 == list.size() );
		
		ArrayList<DockingStation> afterDS = testDS.getDockingStations(null);
		
		for (int i =0 ; i< afterDS.size(); i++) {
			DockingStation x = afterDS.get(i);
			for (int j = 0 ; j<x.getId().size(); j++) {
				assertTrue("Add success",singlebike.getIdDockingStation().equals(x.getId().get(j)));
			}
			

			
		}
	}
}
