package adminmain;

import java.awt.BorderLayout;

import javax.swing.*;

@SuppressWarnings("serial")
public class EBRAdmin extends JFrame {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;
	
	public EBRAdmin(EBRAdminController controller) {
		dispose();
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);
		
		
		//rootPanel.add(controller.getCartPane(), BorderLayout.NORTH);
		
		
		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);
		
		
		
		JPanel singleBikePage = controller.getSingleBikePage();
		tabbedPane.addTab("Quan li xe", null, singleBikePage, "Xe dap don");
		
		JPanel singleEBikePage = controller.getSingleEBikePage();
		tabbedPane.addTab("Quan li xe", null, singleEBikePage, "Xe dap dien don");
		
		JPanel twinBikePage = controller.getTwinBikePage();
		tabbedPane.addTab("Quan li xe", null, twinBikePage, "Xe dap doi");
		
		JPanel DSPage = controller.getDockingStationPage();
		tabbedPane.addTab("Quan li bai xe", null, DSPage, "Quan li bai xe");
		



		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Eco Bike Rental for Admin");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new EBRAdmin(new EBRAdminController());
			}
		});
	}
}
