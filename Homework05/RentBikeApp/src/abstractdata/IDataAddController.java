package abstractdata;

import java.util.Map;

public interface IDataAddController {
	public void add(Map<String, String> addParams);
}
