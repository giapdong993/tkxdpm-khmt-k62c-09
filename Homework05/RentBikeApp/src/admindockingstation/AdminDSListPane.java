package admindockingstation;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;

import com.ebr.bean.DockingStation;
import com.ebr.ecobike.controller.AdminEcoBikePageController;

import abstractdata.ADataListPane;
import abstractdata.ADataPageController;
import abstractdata.ADataSinglePane;

@SuppressWarnings("serial")
public class AdminDSListPane extends ADataListPane<DockingStation> {

	public List<DockingStation> deleteDockingStation;
	public AdminDSListPane(ADataPageController<DockingStation> controller) {
		this.controller = controller;
	}
	

	@Override
	public void decorateSinglePane(ADataSinglePane<DockingStation> singlePane) {
		
		
		JButton button = new JButton("Edit");
		singlePane.addDataHandlingComponent(button);
		button.setBackground(Color.BLUE);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//if (controller instanceof UserMediaPageController) {
				//	((UserMediaPageController) controller).addToCart(singlePane.getData().getId(), singlePane.getData().getTitle(), singlePane.getData().getCost(), (int)spin.getValue());
				//}
			}
		});
		JButton button1 = new JButton("Delete");
		singlePane.addDataHandlingComponent(button1);
		button1.setBackground(Color.BLUE);
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (controller instanceof AdminDSPageController) {

				}
			}
		});
		
	}
	
}
