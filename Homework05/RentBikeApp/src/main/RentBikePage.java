package main;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;
//import javax.swing.border.Border;

import com.ebr.bean.EcoBike;

@SuppressWarnings("serial")
public class RentBikePage extends JFrame {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;

	public RentBikePage() {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);

		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);

		JPanel homePage = createView();
		tabbedPane.addTab("Rent bike", null, homePage, "Rent bike page");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Rent bike application");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	private JPanel createView() {
		JPanel panel = new JPanel();
		panel.setSize(100, 200);

		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		JButton rentBikeButton = new JButton("Rent");
		JButton cancelButton = new JButton("Cancel");
		JTextField textFieldCode = new JTextField(15);
		JLabel labelCode = new JLabel("Bike code");
		gbc.fill = GridBagConstraints.HORIZONTAL;

		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(labelCode, gbc);

		gbc.gridx = 1;
		gbc.gridy = 0;
		panel.add(textFieldCode, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		panel.add(cancelButton, gbc);
		gbc.gridx = 1;
		gbc.gridy = 1;
		panel.add(rentBikeButton, gbc);

		rentBikeButton.addActionListener(new ActionSearchBikeById(textFieldCode));
		cancelButton.addActionListener(new ActionCancel());

		return panel;
	}

	public class ActionSearchBikeById implements ActionListener {
		private JTextField fieldBikeId;

		public ActionSearchBikeById(JTextField fieldBikeId) {
			this.fieldBikeId = fieldBikeId;
		}

		public void actionPerformed(ActionEvent e) {
			ArrayList<EcoBike> res = RentBikeController.singleton().searchBikeById(fieldBikeId.getText());
			if (res.size() == 0) {
				JOptionPane.showMessageDialog(null, "Bạn đã nhập sai mã xe!");
				return;
			}
			
			fieldBikeId.setText(null);
			
			System.out.println("APP: " + fieldBikeId.getText() + " => " + res);
			RentBikeController.singleton().gotoPaymentPage(res.get(0).getCost());
		}
	}
	
	public class ActionCancel implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			RentBikeController.singleton().gotoHomePage();
		}
		
	}
}
