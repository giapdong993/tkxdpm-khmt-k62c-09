package main;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;

import javax.swing.*;
import com.ebr.bean.CreditCard;

@SuppressWarnings("serial")
public class PaymentPage extends JFrame {
	private PaymentController controller;
	
	public JTextField textFieldCost;
	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;
	
	public PaymentPage(PaymentController controller) {
		this.controller = controller;

		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);

		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);

		JPanel homePage = createView();
		tabbedPane.addTab("Payment", null, homePage, "Payment page");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Rent bike application");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
		addComponentListener(new winlistenner(this));
	}

	private JPanel createView() {
		JPanel panel = new JPanel();
		panel.setSize(100, 200);

		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		JButton paymentButton = new JButton("Payment");
		JButton cancelButton = new JButton("Cancel");
		JLabel labelCost = new JLabel("Cost");
		JTextField textFieldCost = new JTextField(Integer.toString(controller.getCost()), 15);
		this.textFieldCost = textFieldCost;

		JLabel labelNumberCreditCard = new JLabel("Number of credit card");
		JTextField textFieldNumberCreditCard = new JTextField(15);

		JLabel labelDateExperied = new JLabel("Date experied");
		JTextField textFieldDateExperied = new JTextField(15);

		JLabel labelNumberSecret = new JLabel("Number secret");
		JTextField textFieldNumberSecret = new JTextField(15);
		gbc.fill = GridBagConstraints.HORIZONTAL;

		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(labelCost, gbc);
		textFieldCost.setEnabled(false);
		gbc.gridx = 1;
		gbc.gridy = 0;
		panel.add(textFieldCost, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		panel.add(labelNumberCreditCard, gbc);
		gbc.gridx = 1;
		gbc.gridy = 1;
		panel.add(textFieldNumberCreditCard, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		panel.add(labelDateExperied, gbc);
		gbc.gridx = 1;
		gbc.gridy = 2;
		panel.add(textFieldDateExperied, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		panel.add(labelNumberSecret, gbc);
		gbc.gridx = 1;
		gbc.gridy = 3;
		panel.add(textFieldNumberSecret, gbc);

		gbc.gridx = 0;
		gbc.gridy = 5;
		panel.add(cancelButton, gbc);
		gbc.gridx = 1;
		gbc.gridy = 5;
		panel.add(paymentButton, gbc);

		paymentButton.addActionListener(
				new ActionPayment(textFieldNumberCreditCard, textFieldDateExperied, textFieldNumberSecret));
		cancelButton.addActionListener(new ActionCancel());

		return panel;
	}

	public class ActionPayment implements ActionListener {
		private JTextField textFieldNumberCreditCard;
		private JTextField textFieldDateExperied;
		private JTextField textFieldNumberSecret;

		public ActionPayment(JTextField textFieldNumberCreditCard, JTextField textFieldDateExperied,
				JTextField textFieldNumberSecret) {
			this.textFieldNumberCreditCard = textFieldNumberCreditCard;
			this.textFieldDateExperied = textFieldDateExperied;
			this.textFieldNumberSecret = textFieldNumberSecret;
		}

		public void actionPerformed(ActionEvent e) {
			CreditCard card = new CreditCard(null, textFieldNumberCreditCard.getText(), textFieldDateExperied.getText(),
					textFieldNumberSecret.getText(), controller.getCost());

			ArrayList<CreditCard> res = controller.deductionPayment(card);
			if (res.size() == 0) {
				JOptionPane.showMessageDialog(null, "Giao dịch thất bại!");
				return;
			}
			JOptionPane.showMessageDialog(null, "Giao dịch thành công!");
			controller.gotoHomePage();
		}
	}

	public class ActionCancel implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			controller.gotoHomePage();
		}

	}
	
	public class winlistenner implements ComponentListener {
		private PaymentPage refPage;
		
		public winlistenner(PaymentPage refPage) {
			this.refPage = refPage;
		}
		

        public void componentHidden(ComponentEvent arg0) {
            // TODO Auto-generated method stub
            System.out.print("Hided\r\n");

        }

        public void componentMoved(ComponentEvent arg0) {
            // TODO Auto-generated method stub
            System.out.print("Moved\r\n");

        }

        public void componentResized(ComponentEvent arg0) {
            // TODO Auto-generated method stub
            System.out.print("Resized\r\n");


        }

        public void componentShown(ComponentEvent arg0) {
            // TODO Auto-generated method stub

            System.out.print("Shown\r\n");
            refPage.textFieldCost.setText(Integer.toString(controller.getCost()));

        }

}
}
