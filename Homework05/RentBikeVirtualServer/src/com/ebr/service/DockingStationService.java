package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.DockingStation;
import com.ebr.db.IMediaDatabase;
import com.ebr.db.JsonMediaDatabase;

@Path("/DockingStation")
public class DockingStationService {

	private IMediaDatabase mediaDatabase;

	public DockingStationService() {
		mediaDatabase = JsonMediaDatabase.singleton();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<DockingStation> getDockingStations(@QueryParam("stationId") String stationId, @QueryParam("stationName") String stationName,
			@QueryParam("stationAddress") String stationAddress, @QueryParam("numberOfAllDocks") int numberOfAllDocks,
			@QueryParam("numberOfBikes") int numberOfBikes, @QueryParam("numberOfEBikes") int numberOfEBikes, @QueryParam("numberOfTwinBikes") int numberOfTwinBikes){
		DockingStation ds = new DockingStation(stationId, stationName, stationAddress,numberOfAllDocks,numberOfBikes,numberOfEBikes,numberOfTwinBikes,"dockingstation",null);
		ds.setStationId(stationId);
		ds.setStationName(stationName);
		ds.setStationAddress(stationAddress);
		ArrayList<DockingStation> res = mediaDatabase.searchDockingStation(ds);
		return res;
	}

	@POST
	@Path("/{stationId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<DockingStation> addDockingStation(@PathParam("stationId") String stationId,DockingStation ds) {
		ArrayList<DockingStation> res = mediaDatabase.addDockingStation(ds);
		return res;
	}

	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<DockingStation> deleteDockingStation(DockingStation ds) {
		ArrayList<DockingStation> res = mediaDatabase.deleteDockingStation(ds);
		return res;
	}
}
