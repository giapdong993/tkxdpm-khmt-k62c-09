package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.DockingStation;
import com.ebr.bean.EcoBike;

import com.ebr.db.IMediaDatabase;
import com.ebr.db.JsonMediaDatabase;

@Path("/RentBike")
public class RentBikeService {

	private IMediaDatabase mediaDatabase;

	public RentBikeService() {
		mediaDatabase = JsonMediaDatabase.singleton();
	}

	/*------------------------------------------ For rent bike -------------------------------------*/
	/*------------------------------------------ For rent bike -------------------------------------*/
	/*------------------------------------------ For rent bike -------------------------------------*/

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EcoBike> findBikeByIdForRent(@QueryParam("bikeId") String bikeId) {
		System.out.println("API: Search bike with bikeId: " + bikeId);

		EcoBike bike = new EcoBike(bikeId, "", "", 1, "", null, "", 1, "");

		ArrayList<EcoBike> res = mediaDatabase.findBikeByIdForRent(bike);
		return res;
	}

}
