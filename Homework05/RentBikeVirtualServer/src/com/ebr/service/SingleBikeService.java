package com.ebr.service;

import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.EcoBike;
import com.ebr.bean.SingleBike;

import com.ebr.db.IMediaDatabase;
import com.ebr.db.JsonMediaDatabase;

@Path("/SingleBike")
public class SingleBikeService {

	private IMediaDatabase mediaDatabase;

	public SingleBikeService() {
		mediaDatabase = JsonMediaDatabase.singleton();
	}
	
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EcoBike> getSingleBikes(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("type") String type,@QueryParam("weight") int weight,
			@QueryParam("licensePlate") String licensePlate,
			@QueryParam("manuafacturingDate") Date manuafacturingDate,
			@QueryParam("producer") String producer, @QueryParam("cost") int cost,
			@QueryParam("idDockingStation") String idDockingStation) {
		SingleBike singlebike = new SingleBike(id, name, type,  weight,licensePlate, manuafacturingDate, producer, cost,idDockingStation);
		singlebike.setName(name);
		singlebike.setProducer(producer);
		singlebike.setCost(cost);
		ArrayList<EcoBike> res = mediaDatabase.searchSingleBike(singlebike);
		return res;
	}
	
	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<EcoBike> addSingleBike(@PathParam("id") String id,SingleBike singlebike) {
		ArrayList<EcoBike> res = mediaDatabase.addSingleBike(singlebike);
		return res;
	}

	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<EcoBike> deleteSingleBike(SingleBike singlebike) {
		ArrayList<EcoBike> res = mediaDatabase.deleteSingleBike(singlebike);
		return res;
	}
	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SingleBike updateSingleBike(SingleBike singlebike) {
		SingleBike res = mediaDatabase.updateSingleBike(singlebike);
		return res;
	}
	
	
}