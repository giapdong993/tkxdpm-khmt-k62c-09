package com.ebr.service;


import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.EcoBike;
import com.ebr.bean.SingleBike;
import com.ebr.bean.SingleEBike;

import com.ebr.db.IMediaDatabase;
import com.ebr.db.JsonMediaDatabase;

@Path("/SingleEBike")
public class SingleEBikeService {

	private IMediaDatabase mediaDatabase;

	public SingleEBikeService() {
		mediaDatabase = JsonMediaDatabase.singleton();
	}
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EcoBike> getSingleEBikes(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("type") String type,@QueryParam("weight") int weight,
			@QueryParam("licensePlate") String licensePlate,
			@QueryParam("manuafacturingDate") Date manuafacturingDate,
			@QueryParam("producer") String producer, @QueryParam("cost") int cost,
			@QueryParam("idDockingStation") String idDockingStation,
			@QueryParam("batteryPercentage") int batteryPercentage,
			@QueryParam("loadCycles") int loadCycles,
			@QueryParam("usageTime") int usageTime
			) {
		SingleEBike singlebike = new SingleEBike(id, name, type,  weight,licensePlate, manuafacturingDate, producer, cost,idDockingStation);
		singlebike.setBatteryPercentage(batteryPercentage);
		singlebike.setLoadCycles(loadCycles);
		singlebike.setUsageTime(usageTime);
		
		ArrayList<EcoBike> res = mediaDatabase.searchSingleEBike(singlebike);
		return res;
	}
	
	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<EcoBike> addSingleEBike(@PathParam("id") String id,SingleEBike singlebike) {
		ArrayList<EcoBike> res = mediaDatabase.addSingleEBike(singlebike);
		return res;
	}

	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<EcoBike> deleteSingleEBike(SingleEBike singlebike) {
		ArrayList<EcoBike> res = mediaDatabase.deleteSingleEBike(singlebike);
		return res;
	}
	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SingleEBike updateSingleEBike(SingleEBike singlebike) {
		SingleEBike res = mediaDatabase.updateSingleEBike(singlebike);
		return res;
	}
	
}