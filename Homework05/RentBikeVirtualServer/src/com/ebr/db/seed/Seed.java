package com.ebr.db.seed;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.ebr.bean.CreditCard;
import com.ebr.bean.DockingStation;
import com.ebr.bean.EcoBike;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Seed {
	private ArrayList<EcoBike> singlebikes;
	private ArrayList<EcoBike> twinbikes;
	private ArrayList<EcoBike> singleebikes;
	private ArrayList<CreditCard> creditcards;
	private ArrayList<DockingStation> dockingstations;
	private static Seed singleton = new Seed();
	
	private Seed() {
		start();
	}
	
	public static Seed singleton() {
		return singleton;
	}
	
	private void start() {
		singlebikes = new ArrayList<EcoBike>();
		twinbikes = new ArrayList<EcoBike>();
		singleebikes = new ArrayList<EcoBike>();
		creditcards = new ArrayList<CreditCard>();
		dockingstations = new ArrayList<DockingStation>();
		
		singlebikes.addAll(generateDataFromFile( new File(getClass().getResource("./SingleBike.json").getPath()).toString()));
		singleebikes.addAll(generateDataFromFile( new File(getClass().getResource("./SingleEBike.json").getPath()).toString()));
		twinbikes.addAll(generateDataFromFile( new File(getClass().getResource("./TwinBike.json").getPath()).toString()));
		creditcards.addAll(generateDataFromFileForCreditCard( new File(getClass().getResource("./CreditCard.json").getPath()).toString()));
		dockingstations.addAll(generateDataFromFile1( new File(getClass().getResource("./DockingStation.json").getPath()).toString()));
		
		System.out.println(creditcards);
	}
	
	private ArrayList<? extends EcoBike> generateDataFromFile(String filePath){
		ArrayList<? extends EcoBike> res = new ArrayList<EcoBike>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<EcoBike>>() { });
			
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}
	
	private ArrayList<DockingStation> generateDataFromFile1(String filePath){
		ArrayList<DockingStation> res = new ArrayList<DockingStation>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			res = mapper.readValue(json, new TypeReference<ArrayList<DockingStation>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}
	
	private ArrayList<CreditCard> generateDataFromFileForCreditCard(String filePath){
		ArrayList<CreditCard> res = new ArrayList<CreditCard>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			res = mapper.readValue(json, new TypeReference<ArrayList<CreditCard>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}


	public ArrayList<EcoBike> getEcoBikes() {
		return singlebikes;
	}
	public ArrayList<EcoBike> getSingleEBikes() {
		return singleebikes;
	}
	public ArrayList<EcoBike> getTwinBikes() {
		return twinbikes;
	}
	public ArrayList<CreditCard> getCreditCards() {
		return creditcards;
	}
	
	public ArrayList<DockingStation> getDockingStations() {
		return dockingstations;
	}

	public static void main(String[] args) {
		Seed seed = new Seed();
		seed.start();
	}
}
