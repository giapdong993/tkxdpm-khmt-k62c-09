package com.ebr.db;

import java.util.ArrayList;

import com.ebr.bean.CreditCard;
import com.ebr.bean.DockingStation;
import com.ebr.bean.EcoBike;
import com.ebr.bean.SingleBike;
import com.ebr.bean.SingleEBike;
import com.ebr.bean.TwinBike;

public interface IMediaDatabase {
	public ArrayList<EcoBike> searchSingleBike(EcoBike ecobike);
	public ArrayList<EcoBike> addSingleBike(EcoBike ecobike);
	public ArrayList<EcoBike> deleteSingleBike(EcoBike ecobike);
	public SingleBike updateSingleBike (SingleBike singlebike);
	
	public ArrayList<EcoBike> searchSingleEBike(EcoBike ecobike);
	public ArrayList<EcoBike> addSingleEBike(EcoBike ecobike);
	public ArrayList<EcoBike> deleteSingleEBike(EcoBike ecobike);
	public SingleEBike updateSingleEBike (SingleEBike singleebike);
	
	public ArrayList<EcoBike> searchTwinBike(EcoBike ecobike);
	public ArrayList<EcoBike> addTwinBike(EcoBike ecobike);
	public ArrayList<EcoBike> deleteTwinBike(EcoBike ecobike);
	public TwinBike updateTwinBike (TwinBike twinbike);
	
	public ArrayList<DockingStation> searchDockingStation(DockingStation ds);
	public DockingStation updateDockingStation(DockingStation ds);
	public ArrayList<DockingStation> addDockingStation(DockingStation ds);
	public ArrayList<DockingStation> deleteDockingStation(DockingStation ds);
	
	public ArrayList<EcoBike> findBikeByIdForRent(EcoBike bike);
	
	public ArrayList<CreditCard> getListCreditCard();
	public ArrayList<CreditCard> findCreditCard(CreditCard card);
	public ArrayList<CreditCard> deductionCreditCard(CreditCard card);
	public ArrayList<CreditCard> intensionCreditCard(CreditCard card);
}
