I. Danh sách thành viên trong nhóm và nhiệm vụ

1. Giáp Văn Đông 	| 20173019	| Thuê xe
2. Nguyễn Trung Kiên 	| 20173214	| Trả xe
3. Vũ Đức Trường 	| 20173425	| Tìm kiếm bãi xe
4. Bùi Minh Tuấn	| 20173444	| Thêm xe vào hệ thống
5. Nguyễn Hoàng Thành An| 20172935	| Thay đổi cách tính giá thuê xe
6. Đào Tuấn Dũng	|  20173050	| Quản lý thông tin bãi xe

II. Phân công công việc mỗi người.
1. Giáp Văn Đông
- Tạo repo, đóng private các branch chính là master và develop để tránh lỗi conflict khi nhiều người cùng push vào 1 branch.
- Viết API thanh toán bao gồm: Trừ tiền một tài khoản, Cộng tiền một tài khoản, xem thông tin tài khoản.
- Tạo và viết db cho chức năng thanh toán.
- Tạo và viết lớp Service cho chức năng thanh toán và chức năng thuê xe.
- Tạo Interface tổng quát IApi ở ứng dụng app. Từ đây các interface chịu tránh nhiệm call đến api đều extends từ đây. Đây là làm tổng quát hoá các lớp call API.
- Viết UI cho chức năng thuê xe.
- Viết UI cho chức năng thanh toán.

2. Bùi Minh Tuấn
-Xây dựng giao diện trang admin
-Viết Api về xe bao gồm CRUD 3 loại xe khác nhau
-Tạo DB cho các loại xe
-Viết lớp service bên server
-Thiết kế và lập trình , tối ưu code trong use case mình chọn

3. Đào Tuấn Dũng
- Viết Api thêm bãi xe
- Tạo và viết db, service cho chức năng thêm bãi xe
- Tạo và viết chức năng thêm bãi xe của admin
- Hoàn tiện báo cáo nhóm

4. Nguyễn Trung Kiên
- Xây dựng thiết kế cho use-case trả xe.

