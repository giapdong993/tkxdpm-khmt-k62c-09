package com.ebr.db;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.ebr.bean.CreditCard;
import com.ebr.bean.DockingStation;
import com.ebr.bean.EcoBike;
import com.ebr.bean.SingleBike;
import com.ebr.bean.SingleEBike;
import com.ebr.bean.TwinBike;
import com.ebr.db.seed.Seed;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonMediaDatabase implements IMediaDatabase {

	private ArrayList<EcoBike> ecobikes = Seed.singleton().getEcoBikes();
	private ArrayList<EcoBike> singleebikes = Seed.singleton().getSingleEBikes();
	private ArrayList<EcoBike> twinbikes = Seed.singleton().getTwinBikes();
	private ArrayList<CreditCard> listCreditCard = Seed.singleton().getCreditCards();
	private ArrayList<DockingStation> dockingstations = Seed.singleton().getDockingStations();

	private static IMediaDatabase singleton = new JsonMediaDatabase();

	private JsonMediaDatabase() {
	}

	public static IMediaDatabase singleton() {
		return singleton;
	}

	@Override
	public ArrayList<EcoBike> searchSingleBike(EcoBike ecobike) {
		ArrayList<EcoBike> res = new ArrayList<EcoBike>();
		for (EcoBike b : ecobikes) {

			res.add(b);
			b.setType("singlebike");

		}
		return res;
	}

	@Override
	public ArrayList<EcoBike> addSingleBike(EcoBike ecobike) {
		for (EcoBike m : ecobikes) {
			if (m.equals(ecobike)) {
				return null;
			}
			m.setType("singlebike");
		}

		ecobikes.add(ecobike);
		for (DockingStation ds : dockingstations) {
			String dsBike = ecobike.getIdDockingStation();
			if (dsBike.equals(ds.getStationId())) {
				int numberSingleBike = ds.getNumberOfBikes();
				ds.setNumberOfBikes(numberSingleBike + 1);

				ArrayList<String> idSingleBike = ds.getId();
				idSingleBike.add(ecobike.getId());
			}
			ds.setType("dockingstation");
		}
		ObjectMapper mapper = new ObjectMapper();
		File file = new File("./src/com/ebr/db/seed/SingleBike.json");

		File dockingFile = new File("./src/com/ebr/db/seed/DockingStation.json");
		try {
			mapper.writeValue(file, ecobikes);
		} catch (IOException e) {
			e.printStackTrace();

		}

		try {
			mapper.writeValue(dockingFile, dockingstations);
		} catch (IOException e) {
			e.printStackTrace();

		}

		return ecobikes;
	}

	@Override
	public ArrayList<EcoBike> deleteSingleBike(EcoBike ecobike) {
		for (EcoBike m : ecobikes) {
			if (m.equals(ecobike)) {
				ecobikes.remove(ecobike);
				break;
			}
		}

		ObjectMapper mapper = new ObjectMapper();
		File file = new File("./src/com/ebr/db/seed/SingleBike.json");

		try {
			mapper.writeValue(file, ecobikes);
		} catch (IOException e) {
			e.printStackTrace();

		}

		return ecobikes;
	}

	@Override
	public ArrayList<EcoBike> searchSingleEBike(EcoBike ecobike) {
		ArrayList<EcoBike> res = new ArrayList<EcoBike>();
		for (EcoBike b : singleebikes) {

			res.add(b);
			b.setType("singleebike");

		}
		return res;
	}

	@Override
	public ArrayList<EcoBike> addSingleEBike(EcoBike ecobike) {
		for (EcoBike m : singleebikes) {
			if (m.equals(ecobike)) {
				return null;
			}
			m.setType("singleebike");
		}

		singleebikes.add(ecobike);

		ObjectMapper mapper = new ObjectMapper();
		File file = new File("./src/com/ebr/db/seed/SingleEBike.json");

		try {
			mapper.writeValue(file, singleebikes);
		} catch (IOException e) {
			e.printStackTrace();

		}

		return singleebikes;
	}

	@Override
	public ArrayList<EcoBike> deleteSingleEBike(EcoBike ecobike) {
		for (EcoBike m : singleebikes) {
			if (m.equals(ecobike)) {
				singleebikes.remove(ecobike);
				break;
			}
		}

		ObjectMapper mapper = new ObjectMapper();
		File file = new File("./src/com/ebr/db/seed/SingleEBike.json");

		try {
			mapper.writeValue(file, singleebikes);
		} catch (IOException e) {
			e.printStackTrace();

		}

		return singleebikes;
	}

	@Override
	public ArrayList<EcoBike> searchTwinBike(EcoBike ecobike) {
		ArrayList<EcoBike> res = new ArrayList<EcoBike>();
		for (EcoBike b : twinbikes) {

			res.add(b);
			b.setType("twinbike");

		}
		return res;
	}

	@Override
	public ArrayList<EcoBike> addTwinBike(EcoBike ecobike) {
		for (EcoBike m : twinbikes) {
			if (m.equals(ecobike)) {
				return null;
			}
			m.setType("twinbike");
		}

		twinbikes.add(ecobike);

		ObjectMapper mapper = new ObjectMapper();
		File file = new File("./src/com/ebr/db/seed/TwinBike.json");

		try {
			mapper.writeValue(file, twinbikes);
		} catch (IOException e) {
			e.printStackTrace();

		}

		return twinbikes;
	}

	@Override
	public ArrayList<EcoBike> deleteTwinBike(EcoBike ecobike) {
		for (EcoBike m : twinbikes) {
			if (m.equals(ecobike)) {
				twinbikes.remove(ecobike);
				break;
			}
		}

		ObjectMapper mapper = new ObjectMapper();
		File file = new File("./src/com/ebr/db/seed/TwinBike.json");

		try {
			mapper.writeValue(file, twinbikes);
		} catch (IOException e) {
			e.printStackTrace();

		}

		return twinbikes;
	}

	@Override
	public ArrayList<DockingStation> searchDockingStation(DockingStation ds) {
		ArrayList<DockingStation> res = new ArrayList<DockingStation>();
		for (DockingStation b : dockingstations) {
			if (b.match(ds)) {
				res.add(b);
				b.setType("dockingstation");
			}
		}
		return res;
	}

	@Override
	public ArrayList<DockingStation> addDockingStation(DockingStation ds) {
		for (DockingStation m : dockingstations) {
			if (m.equals(ds)) {
				return null;
			}
		}

		dockingstations.add(ds);
		ObjectMapper mapper = new ObjectMapper();
		File file = new File("./src/com/ebr/db/seed/DockingStation.json");

		try {
			mapper.writeValue(file, dockingstations);
		} catch (IOException e) {
			e.printStackTrace();

		}

		return dockingstations;
	}

	@Override
	public ArrayList<DockingStation> deleteDockingStation(DockingStation ds) {
		for (DockingStation m : dockingstations) {
			if (m.equals(ds)) {
				dockingstations.remove(ds);
				break;
			}
		}

		ObjectMapper mapper = new ObjectMapper();
		File file = new File("./src/com/ebr/db/seed/DockingStation.json");

		try {
			mapper.writeValue(file, dockingstations);
		} catch (IOException e) {
			e.printStackTrace();

		}

		return dockingstations;
	}

	@Override
	public DockingStation updateDockingStation(DockingStation ds) {
		for (DockingStation m : dockingstations) {
			if (m.equals(ds)) {
				dockingstations.remove(m);
				dockingstations.add(ds);
				return ds;
			}
		}
		return null;
	}

	@Override
	public ArrayList<EcoBike> findBikeByIdForRent(EcoBike bike) {
		ArrayList<EcoBike> res = new ArrayList<EcoBike>();
		boolean found = false;

		// Search in "singlebike"
		if (!found) {
			for (EcoBike b : ecobikes) {
				boolean isCompare = b.getId().equals(bike.getId());

				if (isCompare) {
					b.setType("singlebike");
					res.add(b);
					found = true;
					break;
				}
			}
		}

		// Search in "singleebike"
		if (!found) {
			for (EcoBike b : singleebikes) {
				boolean isCompare = b.getId().equals(bike.getId());

				if (isCompare) {
					b.setType("singleebike");
					res.add(b);
					found = true;
					break;
				}
			}
		}

		// Search in "twinbikes"
		if (!found) {
			for (EcoBike b : twinbikes) {
				boolean isCompare = b.getId().equals(bike.getId());

				if (isCompare) {
					b.setType("twinbike");
					res.add(b);
					found = true;
					break;
				}
			}
		}

		return res;
	}

	@Override
	public ArrayList<CreditCard> getListCreditCard() {

		ArrayList<CreditCard> res = new ArrayList<CreditCard>();
		for (CreditCard b : listCreditCard) {
			res.add(b);
		}
		return res;
	}

	@Override
	public ArrayList<CreditCard> findCreditCard(CreditCard card) {
		ArrayList<CreditCard> res = new ArrayList<CreditCard>();
		for (CreditCard b : listCreditCard) {
			boolean isCompare = b.getCardNumber().equals(card.getCardNumber());

			if (isCompare) {
				res.add(b);
				break;
			}
		}
		return res;
	}

	@Override
	public ArrayList<CreditCard> deductionCreditCard(CreditCard card) {
		boolean success = true;
		boolean matchCard = false;
		for (CreditCard m : listCreditCard) {
			if (m.equals(card)) {
				matchCard = true;
				if (m.getTotalMoney() >= card.getTotalMoney()) {
					m.setTotalMoney(m.getTotalMoney() - card.getTotalMoney());
					break;
				} else {
					return new ArrayList<CreditCard>();
				}

			}
		}

		if (!matchCard)
			return new ArrayList<CreditCard>();

		if (success) {
			ObjectMapper mapper = new ObjectMapper();
			File file = new File("./src/com/ebr/db/seed/CreditCard.json");

			try {
				mapper.writeValue(file, listCreditCard);
			} catch (IOException e) {
				e.printStackTrace();
				return new ArrayList<CreditCard>();
			}

			return listCreditCard;
		} else {
			return new ArrayList<CreditCard>();
		}
	}

	@Override
	public ArrayList<CreditCard> intensionCreditCard(CreditCard card) {
		boolean success = true;
		boolean matchCard = false;
		for (CreditCard m : listCreditCard) {
			if (m.equals(card)) {
				matchCard = true;
				m.setTotalMoney(m.getTotalMoney() + card.getTotalMoney());
				break;
			}
		}

		if (!matchCard)
			return new ArrayList<CreditCard>();

		if (success) {
			ObjectMapper mapper = new ObjectMapper();
			File file = new File("./src/com/ebr/db/seed/CreditCard.json");

			try {
				mapper.writeValue(file, listCreditCard);
			} catch (IOException e) {
				e.printStackTrace();
				return new ArrayList<CreditCard>();
			}

			return listCreditCard;
		} else {
			return new ArrayList<CreditCard>();
		}
	}

	@Override
	public SingleBike updateSingleBike(SingleBike singlebike) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SingleEBike updateSingleEBike(SingleEBike singleebike) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TwinBike updateTwinBike(TwinBike twinbike) {
		// TODO Auto-generated method stub
		return null;
	}

}
