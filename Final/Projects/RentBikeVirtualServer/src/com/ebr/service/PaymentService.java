package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.CreditCard;

import com.ebr.db.IMediaDatabase;
import com.ebr.db.JsonMediaDatabase;

@Path("/Payment")
public class PaymentService {

	private IMediaDatabase mediaDatabase;

	public PaymentService() {
		mediaDatabase = JsonMediaDatabase.singleton();
	}

	@GET
	@Path("/deduction")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<CreditCard> deductionPayment(@QueryParam("cardNumber") String cardNumber,
			@QueryParam("dateExpired") String dateExpired, @QueryParam("secretNumber") String secretNumber,
			@QueryParam("totalMoney") int totalMoney) {
		CreditCard card = new CreditCard(null, cardNumber, dateExpired, secretNumber, totalMoney);

		if (card.checkValidCard()) {
			ArrayList<CreditCard> res = mediaDatabase.deductionCreditCard(card);
			return res;
		} else {
			return new ArrayList<CreditCard>();
		}
	}

	@GET
	@Path("/intension")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<CreditCard> intensionPayment(@QueryParam("cardNumber") String cardNumber,
			@QueryParam("dateExpired") String dateExpired, @QueryParam("secretNumber") String secretNumber,
			@QueryParam("totalMoney") int totalMoney) {
		CreditCard card = new CreditCard("", cardNumber, dateExpired, secretNumber, totalMoney);

		if (card.checkValidCard()) {
			ArrayList<CreditCard> res = mediaDatabase.intensionCreditCard(card);
			return res;
		} else {
			return new ArrayList<CreditCard>();
		}
	}

	@GET
	@Path("/view")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<CreditCard> searchCreditCard(@QueryParam("cardNumber") String cardNumber) {
		CreditCard card = new CreditCard();
		card.setCardNumber(cardNumber);
		ArrayList<CreditCard> res = mediaDatabase.findCreditCard(card);
		return res;
	}
}
