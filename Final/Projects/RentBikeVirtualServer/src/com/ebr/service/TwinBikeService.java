package com.ebr.service;

import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.EcoBike;
import com.ebr.bean.SingleBike;
import com.ebr.bean.TwinBike;

import com.ebr.db.IMediaDatabase;
import com.ebr.db.JsonMediaDatabase;

@Path("/TwinBike")
public class TwinBikeService {

	private IMediaDatabase mediaDatabase;

	public TwinBikeService() {
		mediaDatabase = JsonMediaDatabase.singleton();
	}
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EcoBike> getTwinBikes(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("type") String type,@QueryParam("weight") int weight,
			@QueryParam("licensePlate") String licensePlate,
			@QueryParam("manuafacturingDate") Date manuafacturingDate,
			@QueryParam("producer") String producer, @QueryParam("cost") int cost,
			@QueryParam("idDockingStation") String idDockingStation) {
		TwinBike twinbike = new TwinBike(id, name, type,  weight,licensePlate, manuafacturingDate, producer, cost,idDockingStation);
		twinbike.setName(name);
		twinbike.setProducer(producer);
		twinbike.setCost(cost);
		ArrayList<EcoBike> res = mediaDatabase.searchTwinBike(twinbike);
		return res;
	}
	
	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<EcoBike> addTwinBike(@PathParam("id") String id,TwinBike twinbike) {
		ArrayList<EcoBike> res = mediaDatabase.addTwinBike(twinbike);
		return res;
	}

	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<EcoBike> deleteTwinBike(TwinBike twinbike) {
		ArrayList<EcoBike> res = mediaDatabase.deleteTwinBike(twinbike);
		return res;
	}
	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TwinBike updateTwinBike(TwinBike twinbike) {
		TwinBike res = mediaDatabase.updateTwinBike(twinbike);
		return res;
	}
}