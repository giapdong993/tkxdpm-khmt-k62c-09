package com.ebr.bean;



import java.util.Date;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;



@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = SingleBike.class, name = "singlebike"), @Type(value = TwinBike.class, name = "twinbike")})
public class Bike extends EcoBike{
	
	public Bike() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Bike(String id, String name, String type, int weight, String licensePlate, Date manuafacturingDate,
			String producer, int cost,String idDockingStation) {
		super(id, name, type, weight, licensePlate, manuafacturingDate, producer, cost,idDockingStation);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
	
	@Override
	public boolean match(EcoBike ecobike) {
		if (ecobike == null)
			return true;
		
		
		boolean res = super.match(ecobike);
		if (!res) {
			return false;
		}
		
		
		if (!(ecobike instanceof Bike))
			return false;
		Bike bike = (Bike) ecobike;
		return true;
	}
}
