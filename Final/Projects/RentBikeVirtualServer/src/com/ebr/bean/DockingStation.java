package com.ebr.bean;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("dockingstation")

public class DockingStation {
	private String type;
	private String stationId;
	private String stationName;
	private String stationAddress;
	private int numberOfAllDocks;
	private int numberOfBikes;
	private int numberOfEBikes;
	private int numberOfTwinBikes;
	private ArrayList<String> idSingleBike;
	

	public DockingStation() {
		super();


	}
	public DockingStation(String stationId, String stationName, String stationAddress, int numberOfAllDocks,
			int numberOfBikes, int numberOfEBikes, int numberOfTwinBikes, String type, ArrayList<String> idSingleBike) {
		this.type = type;
		this.stationId = stationId;
		this.stationName = stationName;
		this.stationAddress = stationAddress;
		this.numberOfAllDocks = numberOfAllDocks;
		this.numberOfBikes = numberOfBikes;
		this.numberOfEBikes = numberOfEBikes;
		this.numberOfTwinBikes = numberOfTwinBikes;
		this.idSingleBike = idSingleBike;		
	}
	
	
	public ArrayList<String> getId() {
		return idSingleBike;
	}
	public void setId(ArrayList<String> idSingleBike) {
		this.idSingleBike = idSingleBike;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStationId() {
		return stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getStationAddress() {
		return stationAddress;
	}

	public void setStationAddress(String stationAddress) {
		this.stationAddress = stationAddress;
	}

	public int getNumberOfAllDocks() {
		return numberOfAllDocks;
	}

	public void setNumberOfAllDocks(int numberOfAllDocks) {
		this.numberOfAllDocks = numberOfAllDocks;
	}

	public int getNumberOfBikes() {
		return numberOfBikes;
	}

	public void setNumberOfBikes(int numberOfBikes) {
		this.numberOfBikes = numberOfBikes;
	}

	public int getNumberOfEBikes() {
		return numberOfEBikes;
	}

	public void setNumberOfEBikes(int numberOfEBikes) {
		this.numberOfEBikes = numberOfEBikes;
	}

	public int getNumberOfTwinBikes() {
		return numberOfTwinBikes;
	}

	public void setNumberOfTwinBikes(int numberOfTwinBikes) {
		this.numberOfTwinBikes = numberOfTwinBikes;
	}
	
	@Override
	public String toString() {
		return "stationId: " + this.stationId + ", stationName: " + this.stationName + ", stationAddress " + this.stationAddress + ", numberOfAllDocks " + this.numberOfAllDocks 
				+ ", numberOfBikes: " + this.numberOfBikes + ", numberOfEBikes: " + this.numberOfEBikes + ", numberOfTwinBikes: " + this.numberOfTwinBikes +", id" + this.idSingleBike;
	}
	
	
	public boolean match(DockingStation dockingStation) {
		if (dockingStation == null)
			return true;	
		
		if (dockingStation.stationId != null && !dockingStation.stationId.equals("") && !this.stationId.contains(dockingStation.stationId)) {
			return false;
		}
		if (dockingStation.stationName != null && !dockingStation.stationName.equals("") && !this.stationName.contains(dockingStation.stationName)) {
			return false;
		}
		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DockingStation) {
			return this.stationId.equals(((DockingStation) obj).stationId);
		}
		return false;
	}

}
