package com.ebr.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("ecobike")
@JsonSubTypes({ @Type(value = Bike.class, name = "bike"),@Type(value = EBike.class, name = "ebike") })
public class EcoBike {
	private String id;
	private String name;
	private String type;
	private int weight;
	private String licensePlate;
	private Date manuafacturingDate;
	private String producer;
	private int cost;
	private String idDockingStation;
	
	public EcoBike() {
		super();


	}
	public EcoBike(String id, String name, String type, int weight, String licensePlate, Date manuafacturingDate,
			String producer, int cost, String idDockingStation) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.weight = weight;
		this.licensePlate = licensePlate;
		this.manuafacturingDate = manuafacturingDate;
		this.producer = producer;
		this.cost = cost;
		this.idDockingStation = idDockingStation;
	}
	

	public String getIdDockingStation() {
		return idDockingStation;
	}
	public void setIdDockingStation(String idDockingStation) {
		this.idDockingStation = idDockingStation;
	}
	public String getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
	public int getWeight() {
		return weight;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public Date getManuafacturingDate() {
		return manuafacturingDate;
	}
	public String getProducer() {
		return producer;
	}
	public int getCost() {
		return cost;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public void setManuafacturingDate(Date manuafacturingDate) {
		this.manuafacturingDate = manuafacturingDate;
	}
	public void setProducer(String producer) {
		this.producer = producer;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	@Override
	public String toString() {
		return "id: " + this.id + ", name: " + this.name + ", type: " + this.type + ", weight: " + this.weight + ", licensePlate: "+ this.licensePlate + ", manuafacturingDate: " + this.manuafacturingDate + ", producer: "+ this.producer+ ", cost: "+ this.cost + ",Id Docking Station" + this.idDockingStation;
	}
	
	public boolean match(EcoBike ecobike) {
		if (ecobike == null)
			return true;
		
		//canthanxulingay
		if (ecobike.id != null && !ecobike.id.equals("") && !this.id.contains(ecobike.id)) {
			return false;
		}
		if (ecobike.name != null && !ecobike.name.equals("") && !this.name.contains(ecobike.name)) {
			return false;
		}
		if (ecobike.type != null && !ecobike.type.equals("") && !this.type.contains(ecobike.type)) {
			return false;
		}
		if (ecobike.cost != 0 && this.cost != ecobike.cost) {
			return false;
		}
		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EcoBike) {
			return this.id.equals(((EcoBike) obj).id);
		}
		return false;
	}
}


