package com.ebr.bean;

import java.util.Date;



public class SingleBike extends Bike {
	
	
	
	public SingleBike() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SingleBike(String id, String name, String type, int weight, String licensePlate, Date manuafacturingDate,
			String producer, int cost,String idDockingStation) {
		super(id, name, type, weight, licensePlate, manuafacturingDate, producer, cost,idDockingStation);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return super.toString();
	}
	
	@Override
	public boolean match(EcoBike ecobike) {
		if (ecobike == null)
			return true;
		
		
		boolean res = super.match(ecobike);
		if (!res) {
			return false;
		}
		
		
		if (!(ecobike instanceof SingleBike))
			return false;
		
		return true;
	}
}