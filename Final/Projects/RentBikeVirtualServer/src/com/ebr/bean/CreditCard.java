package com.ebr.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

//@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
//@JsonTypeName("creditcard")
public class CreditCard {
	@JsonProperty("cardId")
	private String cardId;
	@JsonProperty("cardNumber")
	private String cardNumber;
	@JsonProperty("dateExpired")
	private String dateExpired;
	@JsonProperty("secretNumber")
	private String secretNumber;
	@JsonProperty("totalMoney")
	private int totalMoney;

	public CreditCard() {
	}

	public CreditCard(String cardId, String cardNumber, String dateExpired, String secretNumber, int totalMoney) {
		this.cardId = cardId;
		this.cardNumber = cardNumber;
		this.dateExpired = dateExpired;
		this.secretNumber = secretNumber;
		this.totalMoney = totalMoney;
	}

	public String getCardNumber() {
		return this.cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public int getTotalMoney() {
		return this.totalMoney;
	}

	public void setTotalMoney(int money) {
		this.totalMoney = money;
	}

	@Override
	public String toString() {
		return 	"cardId: " + this.cardId + 				" , " + 
				"cardNumber: " + this.cardNumber + 		" , " + 
				"dateExpired: "	+ this.dateExpired +	" , " + 
				"secretNumber: " + this.secretNumber + 	" , " + 
				"totalMoney: "	+ this.totalMoney;
	}

	public boolean match(CreditCard creditCard) {
		if (creditCard == null)
			return true;

		if (creditCard.cardId != null && !creditCard.cardId.equals("") && !this.cardId.equals(creditCard.cardId)) {
			return false;
		}

		if (creditCard.cardNumber != null && !creditCard.cardNumber.equals("")
				&& !this.cardNumber.equals(creditCard.cardNumber)) {
			return false;
		}

		return true;
	}
	
	public boolean checkValidCard() {
		if(this.cardNumber == null || this.cardNumber.equals("")) return false;
		if(this.dateExpired == null || this.dateExpired.equals("")) return false;
		if(this.secretNumber == null || this.secretNumber.equals("")) return false;
		
		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CreditCard) {
			CreditCard card = (CreditCard) obj;
			boolean matchCardNumber = this.cardNumber.equals(card.cardNumber);
			return matchCardNumber;
		}
		return false;
	}
}