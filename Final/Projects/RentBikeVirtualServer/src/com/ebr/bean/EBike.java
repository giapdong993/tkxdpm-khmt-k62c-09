package com.ebr.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = SingleEBike.class, name = "singleebike")})
public class EBike extends EcoBike{
	private int batteryPercentage;
	private int loadCycles;
	private int usageTime;
	
	

	
	public EBike() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public int getBatteryPercentage() {
		return batteryPercentage;
	}


	public int getLoadCycles() {
		return loadCycles;
	}


	public int getUsageTime() {
		return usageTime;
	}


	public void setBatteryPercentage(int batteryPercentage) {
		this.batteryPercentage = batteryPercentage;
	}


	public void setLoadCycles(int loadCycles) {
		this.loadCycles = loadCycles;
	}


	public void setUsageTime(int usageTime) {
		this.usageTime = usageTime;
	}


	public EBike(String id, String name, String type, int weight, String licensePlate, Date manuafacturingDate,
			String producer, int cost,String idDockingStation) {
		super(id, name, type, weight, licensePlate, manuafacturingDate, producer, cost,idDockingStation);
		// TODO Auto-generated constructor stub
	}


	public EBike(String id, String name, String type, int weight, String licensePlate, Date manuafacturingDate,
			String producer, int cost,int batteryPercentage, int loadCycles, int usageTime,String idDockingStation) {
		super(id, name, type, weight, licensePlate, manuafacturingDate, producer, cost,idDockingStation);
		this.batteryPercentage = batteryPercentage;
		this.loadCycles = loadCycles;
		this.usageTime = usageTime;
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return super.toString() + ", Battery Percentage: " + batteryPercentage + ", loadCycles: " +loadCycles + ", usageTime: " + usageTime ;
	}
	
	@Override
	public boolean match(EcoBike ecobike) {
		if (ecobike == null)
			return true;
		
		
		boolean res = super.match(ecobike);
		if (!res) {
			return false;
		}
		
		
		if (!(ecobike instanceof EBike))
			return false;
		EBike ebike = (EBike) ecobike;
		
		
		
		if (ebike.batteryPercentage != 0 && this.batteryPercentage != ebike.batteryPercentage) {
			return false;
		}
		if (ebike.loadCycles != 0 && this.loadCycles != ebike.loadCycles) {
			return false;
		}
		
		
		return true;
	}
}
