package main;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import com.chargeMethod.controller.InfoChargeMethodPageController;
import com.chargeMethod.view.InfoChargeMethodPage;

@SuppressWarnings("serial")
public class HomePage extends JFrame {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;

	public HomePage() {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);

		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);

		JPanel homePage = createView();
		tabbedPane.addTab("Home", null, homePage, "Home page");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Rent bike application");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	private JPanel createView() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		JButton rentBikeButton = new JButton("Rent bike");
		JButton returnBikeButton = new JButton("Return bike");
		JButton searchBikeButton = new JButton("Search bike");

		//Button An
		JButton chargeMethod = new JButton("Charge Method");
		JButton FindDSButton = new JButton("Find Docking Station");

		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(rentBikeButton, gbc);

		gbc.gridx = 1;
		gbc.gridy = 0;
		panel.add(returnBikeButton, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 2;
		panel.add(searchBikeButton, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 3;
		panel.add(FindDSButton, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 3;
		chargeMethod.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new InfoChargeMethodPage(new InfoChargeMethodPageController());
			}
		});
		panel.add(chargeMethod, gbc);

		rentBikeButton.addActionListener(new ActionGotoRentBike());
		FindDSButton.addActionListener(new ActionGotoFindDockingStation());

		return panel;
	}

	public class ActionGotoRentBike implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			HomeController.singleton().gotoRentBikePage();
		}
	}
	public class ActionGotoFindDockingStation implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			HomeController.singleton().gotoFindDockingstationPage();
		}
	}
}
