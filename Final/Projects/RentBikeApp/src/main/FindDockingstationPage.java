package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.Border;

import com.ebr.bean.DockingStation;
import com.ebr.bean.EcoBike;

import main.RentBikePage.ActionSearchBikeById;

@SuppressWarnings("serial")
public class FindDockingstationPage extends JFrame {
	private FindDockingstationController controller;
	
	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;
	
	public FindDockingstationPage(FindDockingstationController controller) {
		this.controller = controller;
		
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);

		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);

		JPanel homePage = createView();
		tabbedPane.addTab("Find DockingStation", null, homePage, "Find DockingStation page");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Find DockingStation application");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}
	
	private JPanel createView() {
		JPanel panel = new JPanel();
		panel.setSize(100, 200);

		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		JButton findDockingstationButtonById = new JButton("Find DockingStationById");
		JButton findDockingstationButtonByAddress = new JButton("Find DockingStationByAddress");
		JTextField textFieldFindById = new JTextField(15);
		JLabel labelFindById = new JLabel("FindById");
		
		JTextField textFieldFindByAddress = new JTextField(15);
		JLabel labelFindByAddress = new JLabel("FindByAddress");
		gbc.fill = GridBagConstraints.HORIZONTAL;

		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(labelFindById, gbc);

		gbc.gridx = 1;
		gbc.gridy = 0;
		panel.add(textFieldFindById, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		panel.add(labelFindByAddress, gbc);

		gbc.gridx = 1;
		gbc.gridy = 1;
		panel.add(textFieldFindByAddress, gbc);

		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		panel.add(findDockingstationButtonById, gbc);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 2;
		gbc.gridy = 1;
		gbc.gridwidth = 2;
		panel.add(findDockingstationButtonByAddress, gbc);
		
		
		findDockingstationButtonById.addActionListener(new ActionSearchDSById(textFieldFindById));
		findDockingstationButtonByAddress.addActionListener(new ActionSearchDSByAddress(textFieldFindByAddress));
		return panel;
	}
	public class ActionSearchDSById implements ActionListener {
		private JTextField fieldDSId;
		
		public ActionSearchDSById(JTextField fieldBikeId) {
			this.fieldDSId = fieldBikeId;
		}
		
		public void actionPerformed(ActionEvent e) {
			ArrayList<DockingStation> res =  controller.showDockingstationById(fieldDSId.getText());
			if(res.size() == 0) {
				JOptionPane.showMessageDialog(null, "Bạn nhập sai Id bãi gửi xe ");
				return;
			}
			System.out.print("APP: " + fieldDSId.getText() + " => " + res);
		}
	}
	public class ActionSearchDSByAddress implements ActionListener {
		private JTextField fieldDSAddress;
		
		public ActionSearchDSByAddress(JTextField fieldDSAddress) {
			this.fieldDSAddress = fieldDSAddress;
		}
		
		public void actionPerformed(ActionEvent e) {
			ArrayList<DockingStation> res =  controller.showDockingstationByAddress(fieldDSAddress.getText());
			if(res.size() == 0) {
				JOptionPane.showMessageDialog(null, "Bạn nhập sai địa chỉ bãi gửi xe");
				return;
			}
			System.out.print("APP: " + fieldDSAddress.getText() + " => " + res);
		}
	}
}
