package main;

import java.util.ArrayList;

import com.ebr.abstractclass.APageController;
import com.ebr.api.PaymentApi;
import com.ebr.api.RentBikeApi;
import com.ebr.bean.CreditCard;
import com.ebr.bean.EcoBike;

/**
 * Control UI, logic of Payment page
 * 
 * @author Giap Dong
 * @since 1.0
 */
public class PaymentController extends APageController<PaymentPage> {
	private static PaymentController instance;
	private static int cost;

	private PaymentController() {
		PaymentPage page = new PaymentPage(this);
		this.addPage(page);
	}

	/**
	 * Get cost wanna payment
	 * 
	 * @return cost
	 * @since 1.0
	 */
	public int getCost() {
		return cost;

	}

	public void gotoHomePage() {
		HomeController.singleton().showPage();
		this.closePage();
	}

	/**
	 * Implement Singleton design pattern
	 * 
	 * @return PaymentController instance
	 * @since 1.0
	 */
	public static PaymentController singleton(int c) {
		System.out.println("APP: cost => " + c);
		cost = c;
		if (instance == null) {
			synchronized (PaymentController.class) {
				instance = new PaymentController();
			}
		}
		return instance;
	}

	@Override
	public void showPage() {
		this.page.setVisible(true);
	}

	@Override
	public void closePage() {
		this.page.setVisible(false);
	}

	/**
	 * Add instance of Page UI to Controller
	 * 
	 * @param PaymentPage instance
	 * @since 1.0
	 */
	@Override
	public void addPage(PaymentPage page) {
		this.page = page;
	}

	public ArrayList<EcoBike> searchBikeById(String bikeId) {
		// SBXX, SEBXX, TBXX
		ArrayList<EcoBike> res = RentBikeApi.singleton().findBikeByIdForRent(bikeId);
		return res;
	}

	public ArrayList<CreditCard> deductionPayment(CreditCard card) {
		ArrayList<CreditCard> res = PaymentApi.singleton().deductionPayment(card);
		return res;
	}
}
