package main;

import java.util.ArrayList;

import com.ebr.abstractclass.APageController;
import com.ebr.api.DockingStationApi;
import com.ebr.api.FindDockingStationApi;
import com.ebr.api.RentBikeApi;
import com.ebr.bean.DockingStation;
import com.ebr.bean.EcoBike;

public class FindDockingstationController extends APageController<FindDockingstationPage> {
	private static FindDockingstationController instance;

	private FindDockingstationController() {
		FindDockingstationPage page = new FindDockingstationPage(this);
		this.addPage(page);
	}

	public void gotoHomePage() {
		HomeController.singleton().showPage();
		this.closePage();
	}

	/**
	 * Implement Singleton design pattern
	 * 
	 * @return FindDockingstationController instance
	 * @since 1.0
	 */
	public static FindDockingstationController singleton() {
		if (instance == null) {
			synchronized (FindDockingstationController.class) {
				instance = new FindDockingstationController();
			}
		}
		return instance;
	}

	@Override
	public void showPage() {
		this.page.setVisible(true);
	}

	@Override
	public void closePage() {
		this.page.setVisible(false);
	}

	/**
	 * Add instance of Page UI to Controller
	 * 
	 * @param FindDockingstationPage instance
	 * @since 1.0
	 */
	@Override
	public void addPage(FindDockingstationPage page) {
		this.page = page;
	}
	
	public ArrayList<DockingStation> showDockingstationById(String inputFindDS) {
		// SBXX, SEBXX, TBXX
		ArrayList<DockingStation> res = FindDockingStationApi.singleton().findDockingStationById(inputFindDS);
		return res;
	}
	public ArrayList<DockingStation> showDockingstationByAddress(String inputFindDS) {
		// SBXX, SEBXX, TBXX
		ArrayList<DockingStation> res = FindDockingStationApi.singleton().findDockingStationByAddress(inputFindDS);
		return res;
	}
}
