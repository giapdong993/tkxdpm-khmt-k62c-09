package main;

import java.util.ArrayList;

import com.ebr.abstractclass.APageController;
import com.ebr.api.RentBikeApi;
import com.ebr.bean.EcoBike;

/**
 * Control UI, logic of RentBike page
 * 
 * @author Giap Dong
 * @since 1.0
 */
public class RentBikeController extends APageController<RentBikePage> {
	private static RentBikeController instance;

	private RentBikeController() {
		RentBikePage page = new RentBikePage();
		this.addPage(page);
	}

	public void gotoHomePage() {
		HomeController.singleton().showPage();
		this.closePage();
	}

	public void gotoPaymentPage(int cost) {
		PaymentController.singleton(cost).showPage();
		this.closePage();
	}

	/**
	 * Implement Singleton design pattern
	 * 
	 * @return RentBikeController instance
	 * @since 1.0
	 */
	public static RentBikeController singleton() {
		if (instance == null) {
			synchronized (RentBikeController.class) {
				instance = new RentBikeController();
			}
		}
		return instance;
	}

	@Override
	public void showPage() {
		this.page.setVisible(true);
	}

	@Override
	public void closePage() {
		this.page.setVisible(false);
	}

	/**
	 * Add instance of Page UI to Controller
	 * 
	 * @param RentBikePage instance
	 * @since 1.0
	 */
	@Override
	public void addPage(RentBikePage page) {
		this.page = page;
	}

	public ArrayList<EcoBike> searchBikeById(String bikeId) {
		// SBXX, SEBXX, TBXX
		ArrayList<EcoBike> res = RentBikeApi.singleton().findBikeByIdForRent(bikeId);
		return res;
	}
}
