package main;

import com.ebr.abstractclass.APageController;

/**
 * Control UI, logic of Home page
 * 
 * @author Giap Dong
 * @since 1.0
 */
public class HomeController extends APageController<HomePage> {
	private static HomeController instance;

	private HomeController() {
		HomePage page = new HomePage();
		this.addPage(page);
	}

	public void gotoRentBikePage() {
		RentBikeController.singleton().showPage();
		this.closePage();
	}
//	public void gotoFindDockingstationPage() {
//		FindDockingstationController.singleton().showPage();
//		this.closePage();
//	}

	/**
	 * Implement Singleton design pattern
	 * 
	 * @return HomeController instance
	 * @since 1.0
	 */
	public static HomeController singleton() {
		if (instance == null) {
			synchronized (HomeController.class) {
				instance = new HomeController();
			}
		}
		return instance;
	}

	@Override
	public void showPage() {
		this.page.setVisible(true);
	}

	@Override
	public void closePage() {
		this.page.setVisible(false);
	}

	/**
	 * Add instance of Page UI to Controller
	 * 
	 * @param HomePage instance
	 * @since 1.0
	 */
	@Override
	public void addPage(HomePage page) {
		this.page = page;
	}

	public void gotoFindDockingstationPage() {
		FindDockingstationController.singleton().showPage();
		this.closePage();
	}
}
