package com.ebr.api;

import java.util.ArrayList;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.bean.EcoBike;

public class RentBikeApi implements IRentBikeApi {
	private static IRentBikeApi singleton = new RentBikeApi();
	private Client client = ClientBuilder.newClient();

	private RentBikeApi() {

	}

	public static IRentBikeApi singleton() {
		return singleton;
	}

	@Override
	public ArrayList<EcoBike> findBikeByIdForRent(String bikeId) {
		WebTarget webTarget = client.target(PATH).path("RentBike");
		webTarget = webTarget.queryParam("bikeId", bikeId);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<EcoBike> res = response.readEntity(new GenericType<ArrayList<EcoBike>>(){});
//		System.out.println(res);
		return res;
	}

}
