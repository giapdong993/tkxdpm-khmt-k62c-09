package com.ebr.api;

import java.util.ArrayList;
import com.ebr.bean.DockingStation;

public interface IFindDockingStationApi extends IApi {
	public ArrayList<DockingStation> findDockingStationById(String inputFindDS);
	public ArrayList<DockingStation> findDockingStationByAddress(String inputFindDS);
}
