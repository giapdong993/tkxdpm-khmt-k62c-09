package com.ebr.api;

import java.util.ArrayList;

import com.ebr.bean.CreditCard;

public interface IPaymentApi extends IApi {
	public ArrayList<CreditCard> intensionPayment(CreditCard card);
	public ArrayList<CreditCard> deductionPayment(CreditCard card);
}
