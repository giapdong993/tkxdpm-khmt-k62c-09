package com.ebr.api;

import java.util.ArrayList;
import com.ebr.bean.EcoBike;

public interface IRentBikeApi extends IApi {
	public ArrayList<EcoBike> findBikeByIdForRent(String bikeId);
}
