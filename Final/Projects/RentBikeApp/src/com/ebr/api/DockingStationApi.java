package com.ebr.api;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.bean.DockingStation;

public class DockingStationApi {

public static final String PATH = "http://localhost:8081/";
	
	private Client client;
	
	public DockingStationApi() {
		client = ClientBuilder.newClient();
	}
	
	public ArrayList<DockingStation> getAllMedias() {
		WebTarget webTarget = client.target(PATH).path("dockingstation");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<DockingStation> res = response.readEntity(new GenericType<ArrayList<DockingStation>>(){});
		System.out.println(res);
		return res;
	}

	public ArrayList<DockingStation> getDockingStations(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("DockingStation");
		
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<DockingStation> res = response.readEntity(new GenericType<ArrayList<DockingStation>>() {});
		System.out.println(res);
		return res;
	}

	

	public DockingStation updateDockingStation(DockingStation ds) {
		WebTarget webTarget = client.target(PATH).path("DockingStation").path(ds.getStationId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(ds, MediaType.APPLICATION_JSON));
		
		DockingStation res = response.readEntity(DockingStation.class);
		return res;
	}
	
	
	
	public ArrayList<DockingStation> addDockingStation(DockingStation ds) {
		WebTarget webTarget = client.target(PATH).path("DockingStation").path(ds.getStationId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(ds, MediaType.APPLICATION_JSON));
		if (response.getStatus() !=  400) {
			ArrayList<DockingStation> res = response.readEntity(new GenericType<ArrayList<DockingStation>>() {});
			System.out.println(response);
			return res;
		}else {
		
		System.out.println(response);
		return null;
		}
	}
	
	public ArrayList<DockingStation> deleteDockingStation(DockingStation ds) {
		WebTarget webTarget = client.target(PATH).path("DockingStation").path("delete");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(ds, MediaType.APPLICATION_JSON));
		if (response.getStatus() !=  400) {
			ArrayList<DockingStation> res = response.readEntity(new GenericType<ArrayList<DockingStation>>() {});
			System.out.println(response);
			return res;
		}else {
		
		System.out.println(response);
		return null;
		}
	}
}
