package com.ebr.api;

import java.util.ArrayList;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.bean.DockingStation;
import com.ebr.bean.EcoBike;

public class FindDockingStationApi implements IFindDockingStationApi {
	
//	private ArrayList<DockingStation> dockingStations = DockingStationApi.getDockingStations(Map<String,String> queryParams);
	public static final String PATH = "http://localhost:8081/";
	private static IFindDockingStationApi singleton = new FindDockingStationApi();
	private Client client = ClientBuilder.newClient();

	private FindDockingStationApi() {

	}

	public static IFindDockingStationApi singleton() {
		return singleton;
	}
	@Override
	public ArrayList<DockingStation> findDockingStationById(String inputFindDS) {
		WebTarget webTarget = client.target(PATH).path("dockingstation");
		webTarget = webTarget.queryParam("stationId", inputFindDS);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<DockingStation> res = response.readEntity(new GenericType<ArrayList<DockingStation>>(){});
//		System.out.println(res);
		return res;
	}

	@Override
	public ArrayList<DockingStation> findDockingStationByAddress(String inputFindDS) {
		WebTarget webTarget = client.target(PATH).path("dockingstation");
		webTarget = webTarget.queryParam("stationAddress", inputFindDS);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<DockingStation> res = response.readEntity(new GenericType<ArrayList<DockingStation>>(){});
//		System.out.println(res);
		return res;
	}
	
}
