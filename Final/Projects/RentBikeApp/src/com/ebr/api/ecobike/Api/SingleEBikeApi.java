package com.ebr.api.ecobike.Api;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.api.ecobike.interfaceApi.ISingleBikeApi;
import com.ebr.api.ecobike.interfaceApi.ISingleEBikeApi;
import com.ebr.bean.SingleBike;
import com.ebr.bean.SingleEBike;

public class SingleEBikeApi implements ISingleEBikeApi{
	private Client client;
	public static final String PATH = "http://localhost:8081/";
	
	
	
	public SingleEBikeApi() {
		client = ClientBuilder.newClient();
	}
	private static ISingleEBikeApi singleton;
	
	public static synchronized ISingleEBikeApi getSingleton() {
		if (singleton == null)
		singleton = new SingleEBikeApi();
		return singleton;
		}
	
	
	@Override
	public ArrayList<SingleEBike> getSingleEBike(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("SingleEBike");

		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<SingleEBike> res = response.readEntity(new GenericType<ArrayList<SingleEBike>>() {});
		System.out.println(res);
		return res;
	}

	@Override
	public SingleEBike updateSingleEBike(SingleEBike singleebike) {
		WebTarget webTarget = client.target(PATH).path("SingleEBike").path("update");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(singleebike, MediaType.APPLICATION_JSON));
		
		SingleEBike res = response.readEntity(SingleEBike.class);
		System.out.println(response);
		return res;
	}

	@Override
	public ArrayList<SingleEBike> addSingleEBike(SingleEBike singlebike) {
		WebTarget webTarget = client.target(PATH).path("SingleEBike").path(singlebike.getId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(singlebike, MediaType.APPLICATION_JSON));
		if (response.getStatus() !=  400) {
			ArrayList<SingleEBike> res = response.readEntity(new GenericType<ArrayList<SingleEBike>>() {});
			System.out.println(response);
			return res;
		}else {
		
		System.out.println(response);
		return null;
		}
	}

	@Override
	public ArrayList<SingleEBike> deleteSingleEBike(SingleEBike singleebike) {
		WebTarget webTarget = client.target(PATH).path("SingleEBike").path("delete");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(singleebike, MediaType.APPLICATION_JSON));
		if (response.getStatus() ==  200) {
			ArrayList<SingleEBike> res = response.readEntity(new GenericType<ArrayList<SingleEBike>>() {});
			System.out.println(response);
			return res;
		}else {
			
		System.out.println(response);
		return null;
		}
	}
}
