package com.ebr.api.ecobike.interfaceApi;

import java.util.ArrayList;
import java.util.Map;

import com.ebr.bean.SingleBike;
import com.ebr.bean.SingleEBike;

public interface ISingleEBikeApi {
	public ArrayList<SingleEBike> getSingleEBike(Map<String, String> queryParams);
	public SingleEBike updateSingleEBike(SingleEBike singleebike);
	public ArrayList<SingleEBike> addSingleEBike(SingleEBike singlebike);
	public ArrayList<SingleEBike> deleteSingleEBike(SingleEBike singleebike);
}
