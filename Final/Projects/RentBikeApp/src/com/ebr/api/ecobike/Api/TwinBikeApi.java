package com.ebr.api.ecobike.Api;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.api.ecobike.interfaceApi.ITwinBikeApi;
import com.ebr.bean.SingleBike;
import com.ebr.bean.TwinBike;

public class TwinBikeApi implements ITwinBikeApi{
	private Client client;
	public static final String PATH = "http://localhost:8081/";
	
	
	
	public TwinBikeApi() {
		client = ClientBuilder.newClient();
	}
	private static ITwinBikeApi singleton;
	
	public static synchronized ITwinBikeApi getSingleton() {
		if (singleton == null)
		singleton = new TwinBikeApi();
		return singleton;
		}
	
	
	
	@Override
	public ArrayList<TwinBike> getTwinBike(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("TwinBike");

		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<TwinBike> res = response.readEntity(new GenericType<ArrayList<TwinBike>>() {});
		System.out.println(res);
		return res;
	}

	@Override
	public TwinBike updateTwinBike(TwinBike twinbike) {
		WebTarget webTarget = client.target(PATH).path("TwinBike").path("update");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(twinbike, MediaType.APPLICATION_JSON));
		
		TwinBike res = response.readEntity(TwinBike.class);
		System.out.println(response);
		return res;
	}

	@Override
	public ArrayList<TwinBike> addTwinBike(TwinBike twinbike) {
		WebTarget webTarget = client.target(PATH).path("TwinBike").path(twinbike.getId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(twinbike, MediaType.APPLICATION_JSON));
		if (response.getStatus() !=  400) {
			ArrayList<TwinBike> res = response.readEntity(new GenericType<ArrayList<TwinBike>>() {});
			System.out.println(response);
			return res;
		}else {
		
		System.out.println(response);
		return null;
		}
	}

	@Override
	public ArrayList<TwinBike> deleteTwinBike(TwinBike twinbike) {
		WebTarget webTarget = client.target(PATH).path("TwinBike").path("delete");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(twinbike, MediaType.APPLICATION_JSON));
		if (response.getStatus() ==  200) {
			ArrayList<TwinBike> res = response.readEntity(new GenericType<ArrayList<TwinBike>>() {});
			System.out.println(response);
			return res;
		}else {
			
		System.out.println(response);
		return null;
		}
	}
	
}
