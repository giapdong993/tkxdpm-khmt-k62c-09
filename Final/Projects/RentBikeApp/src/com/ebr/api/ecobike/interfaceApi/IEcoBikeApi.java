package com.ebr.api.ecobike.interfaceApi;

import java.util.ArrayList;
import java.util.Map;

import com.ebr.bean.EcoBike;
import com.ebr.bean.SingleBike;
import com.ebr.bean.SingleEBike;
import com.ebr.bean.TwinBike;

public interface IEcoBikeApi  {
	public ArrayList<EcoBike> getAllMedias();

}

