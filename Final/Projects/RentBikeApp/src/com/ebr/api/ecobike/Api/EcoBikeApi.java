package com.ebr.api.ecobike.Api;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.bean.SingleBike;
import com.ebr.bean.TwinBike;

import com.ebr.bean.SingleEBike;
import com.ebr.api.ecobike.interfaceApi.IEcoBikeApi;
import com.ebr.bean.EcoBike;

public class EcoBikeApi implements IEcoBikeApi{
	private Client client;
	public static final String PATH = "http://localhost:8081/";
	
	
	
	public EcoBikeApi() {
		client = ClientBuilder.newClient();
	}
	private static IEcoBikeApi singleton;
	
	public static synchronized IEcoBikeApi getSingleton() {
		if (singleton == null)
		singleton = new EcoBikeApi();
		return singleton;
		}
	
	@Override
	public ArrayList<EcoBike> getAllMedias() {
		WebTarget webTarget = client.target(PATH).path("ecobike");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<EcoBike> res = response.readEntity(new GenericType<ArrayList<EcoBike>>(){});
		System.out.println(res);
		return res;
	}
	
}
