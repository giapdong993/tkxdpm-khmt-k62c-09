package com.ebr.api.ecobike.interfaceApi;

import java.util.ArrayList;
import java.util.Map;

import com.ebr.bean.SingleBike;
import com.ebr.bean.SingleEBike;
import com.ebr.bean.TwinBike;

public interface ITwinBikeApi {
	public ArrayList<TwinBike> getTwinBike(Map<String, String> queryParams);
	public TwinBike updateTwinBike(TwinBike twinbike);
	public ArrayList<TwinBike> addTwinBike(TwinBike twinbike);
	public ArrayList<TwinBike> deleteTwinBike(TwinBike twinbike);
}
