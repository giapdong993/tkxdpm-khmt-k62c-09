package com.ebr.dockingstation;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import abstractdata.ADataAddPane;


@SuppressWarnings("serial")
public class DSAddPane extends ADataAddPane {
	private JTextField stationIdField;
	private JTextField stationNameField;
	private JTextField stationAddressField;
	private JTextField typeField;
	private JTextField numberOfAllDocksField;
	private JTextField numberOfBikesField;
	private JTextField numberOfEBikesField;
	private JTextField numberOfTwinBikeField;

	public DSAddPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		JLabel stationIdLabel = new JLabel("Station Id");
		stationIdField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(stationIdLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(stationIdField, c);
		
		
		JLabel stationNameLabel = new JLabel("Station Name");
		stationNameField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(stationNameLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(stationNameField, c);
		
		JLabel stationAddressLabel = new JLabel("Station Address");
		stationAddressField = new JTextField(30);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(stationAddressLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(stationAddressField, c);
				
/*		JLabel typeLabel = new JLabel("Type");
		typeField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(typeLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(typeField, c);
*/		
		JLabel numberOfAllDocksLabel = new JLabel("Number Of All Docks");
		numberOfAllDocksField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(numberOfAllDocksLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(numberOfAllDocksField, c);
		
		JLabel numberOfBikesLabel = new JLabel("Number Of Bikes");
		numberOfBikesField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(numberOfBikesLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(numberOfBikesField, c);
		
		JLabel numberOfEBikesLabel = new JLabel("Number Of EBikes");
		numberOfEBikesField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(numberOfEBikesLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(numberOfEBikesField, c);
		
		JLabel numberOfTwinBikeLabel = new JLabel("Number Of TwinBike");
		numberOfTwinBikeField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(numberOfTwinBikeLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(numberOfTwinBikeField, c);
	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		if (!stationIdField.getText().trim().equals("")) {
			res.put("stationId", stationIdField.getText().trim());
		}
		if (!stationNameField.getText().trim().equals("")) {
			res.put("stationName", stationNameField.getText().trim());
		}
		if (!stationAddressField.getText().trim().equals("")) {
			res.put("stationAddress", stationAddressField.getText().trim());
		}
		if (!numberOfAllDocksField.getText().trim().equals("")) {
			res.put("numberOfAllDocks", numberOfAllDocksField.getText().trim());
		}
		if (!numberOfBikesField.getText().trim().equals("")) {
			res.put("numberOfBikes", numberOfBikesField.getText().trim());
		}
		if (!numberOfEBikesField.getText().trim().equals("")) {
			res.put("numberOfEBikes", numberOfEBikesField.getText().trim());
		}
		if (!numberOfTwinBikeField.getText().trim().equals("")) {
			res.put("numberOfTwinBike", numberOfTwinBikeField.getText().trim());
		}
		
		return res;
	}
}
