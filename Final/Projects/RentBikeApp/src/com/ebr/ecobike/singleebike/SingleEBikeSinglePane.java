package com.ebr.ecobike.singleebike;

import com.ebr.bean.EcoBike;

import com.ebr.ecobike.gui.EBikeSinglePane;

@SuppressWarnings("serial")
public class SingleEBikeSinglePane extends EBikeSinglePane {
	
	public SingleEBikeSinglePane() {
		super();
	}
	
	public SingleEBikeSinglePane(EcoBike ecobike) {
		this();
		this.t = ecobike;

		displayData();
	}
}
