package com.ebr.ecobike.singleebike;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ebr.api.ecobike.Api.SingleBikeApi;
import com.ebr.api.ecobike.Api.SingleEBikeApi;
import com.ebr.api.ecobike.interfaceApi.ISingleBikeApi;
import com.ebr.api.ecobike.interfaceApi.ISingleEBikeApi;
import com.ebr.bean.EcoBike;
import com.ebr.bean.SingleBike;
import com.ebr.bean.SingleEBike;
import com.ebr.ecobike.controller.AdminEcoBikePageController;
import com.ebr.ecobike.gui.EcoBikeAddPane;
import com.ebr.ecobike.gui.EcoBikeSinglePane;
import com.ebr.ecobike.singlebike.SingleBikeSinglePane;

import abstractdata.ADataSearchPane;



public class AdminSingleEBikePageController extends AdminEcoBikePageController{
	
	
	
	public AdminSingleEBikePageController() {
		super();
	}
	
	

	@Override
	public List<? extends EcoBike> search(Map<String, String> searchParams) {
		ISingleEBikeApi singleBikeApi = SingleEBikeApi.getSingleton();
		return singleBikeApi.getSingleEBike(searchParams);
	}
	
	@Override
	public EcoBikeSinglePane createSinglePane() {
		return new SingleEBikeSinglePane();
	}
	@Override
	public EcoBikeAddPane createAddPane() {
		return new SingleEBikeAddPane();//la book
	}

	
	
	@Override
	public List<? extends EcoBike> add(Map<String, String> addParams) {
		
		 String id = null;
		 String name = null;
		 String type = null;
		 int weight = 0;
		 String licensePlate= null;
		 Date manuafacturingDate = null;
		 String producer= null;
		 int cost = 0;
		 String idDockingStation = null;
		 int batteryPercentage = 0;
		 int loadCycles = 0;
		 int usageTime = 0;
		for (String t: addParams.keySet()){
            String key = t.toString();
            if (key.equals("id")) {
            	id = addParams.get(t).toString();
            }
            if (key.equals("name")) {
            	name = addParams.get(t).toString();
            }
            if (key.equals("type")) {
            	type = addParams.get(t).toString();
            }
            if (key.equals("weight")) {
            	weight = Integer.parseInt(addParams.get(t));
            }
            if (key.equals("licensePlate")) {
            	licensePlate = addParams.get(t).toString();
            }
            if (key.equals("manuafacturingDate")) {
            	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                String dateInString = addParams.get(t).toString();

                try {

                    manuafacturingDate = formatter.parse(dateInString);
                    
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (key.equals("producer")) {
            	producer = addParams.get(t).toString();
            }
            if (key.equals("cost")) {
            	cost =Integer.parseInt(addParams.get(t)) ;
            }
            if(key.equals("idDockingStation")) {
            	idDockingStation = addParams.get(t).toString();
            }
            if(key.equals("batteryPercentage")) {
            	batteryPercentage = Integer.parseInt(addParams.get(t));
            }
            if(key.equals("loadCycles")) {
            	loadCycles = Integer.parseInt(addParams.get(t));
            }
            if(key.equals("usageTime")) {
            	usageTime = Integer.parseInt(addParams.get(t));
            }
            String value = addParams.get(t).toString();  
            System.out.println(key + " " + value);
            
		}
		
		SingleEBike addSingleEBike = new SingleEBike (id,name,type,weight,licensePlate,manuafacturingDate,producer,cost,idDockingStation);
		addSingleEBike.setBatteryPercentage(batteryPercentage);
		addSingleEBike.setLoadCycles(loadCycles);
		addSingleEBike.setUsageTime(usageTime);
		
		if (addSingleEBike.getId() !=  null) {
			ISingleEBikeApi singleEBikeApi = SingleEBikeApi.getSingleton();
			return singleEBikeApi.addSingleEBike(addSingleEBike);
		}else {
			return null;
		}
		
	}



	@Override
	public ADataSearchPane createSearchPane() {
		// TODO Auto-generated method stub
		return null;
	}

}

