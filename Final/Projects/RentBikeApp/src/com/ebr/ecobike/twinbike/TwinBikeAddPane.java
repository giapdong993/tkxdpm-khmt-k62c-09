package com.ebr.ecobike.twinbike;

import java.util.Map;

import com.ebr.ecobike.gui.BikeAddPane;

@SuppressWarnings("serial")
public class TwinBikeAddPane extends BikeAddPane{
	public TwinBikeAddPane() {
		super();
	} 
	
	@Override
	public void buildControls() {
		super.buildControls();
		
	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		return res;
	}
}
