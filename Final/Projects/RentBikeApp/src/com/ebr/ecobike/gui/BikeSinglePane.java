package com.ebr.ecobike.gui;



import javax.swing.JLabel;

import com.ebr.bean.EcoBike;
import com.ebr.bean.Bike;

@SuppressWarnings("serial")
public class BikeSinglePane extends EcoBikeSinglePane{
	
	public BikeSinglePane() {
		super();
	}
	
	public BikeSinglePane(EcoBike ecobike) {
		this();
		this.t = ecobike;
		
		displayData();
	}
	
	@Override
	public void buildControls() {
		super.buildControls();

	}
	
	@Override
	public void displayData() {
		super.displayData();
	}
}

