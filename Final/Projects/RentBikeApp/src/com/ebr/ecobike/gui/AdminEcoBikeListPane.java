package com.ebr.ecobike.gui;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.ebr.api.ecobike.Api.SingleBikeApi;
import com.ebr.api.ecobike.interfaceApi.ISingleBikeApi;
import com.ebr.bean.EcoBike;
import com.ebr.bean.SingleBike;
import com.ebr.ecobike.controller.AdminEcoBikePageController;
import com.oms.bean.Media;


import abstractdata.ADataListPane;
import abstractdata.ADataPageController;
import abstractdata.ADataSinglePane;
import abstractdata.IDataManageController;

@SuppressWarnings("serial")
public class AdminEcoBikeListPane extends ADataListPane<EcoBike>{
	public List<? extends EcoBike> deleteEcoBike;
	public AdminEcoBikeListPane(ADataPageController<EcoBike> controller) {
		this.controller = controller;
	}
	

	@Override
	public void decorateSinglePane(ADataSinglePane<EcoBike> singlePane) {
		
		JButton button = new JButton("Edit");
		singlePane.addDataHandlingComponent(button);
		
		IDataManageController<EcoBike> manageController = new IDataManageController<EcoBike>() {
			@Override
			public void onAct(EcoBike t) {
				if (controller instanceof AdminEcoBikePageController) {
					EcoBike newMedia = ((AdminEcoBikePageController) controller).updateEcoBike(t);
					singlePane.updateData(newMedia);
				}
			}


			
		};
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new EcoBikeEditDialog(singlePane.getData(), manageController);
			}
		});	
		
		JButton button1 = new JButton("Delete");
		singlePane.addDataHandlingComponent(button1);
		button1.setBackground(Color.BLUE);
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (controller instanceof AdminEcoBikePageController) {
					deleteEcoBike = 
					controller.delete(singlePane.getData().getId(),
							singlePane.getData().getName(),singlePane.getData().getType(),
							singlePane.getData().getWeight(),singlePane.getData().getLicensePlate(),
							singlePane.getData().getManuafacturingDate(),singlePane.getData().getProducer(),
							singlePane.getData().getCost(), singlePane.getData().getIdDockingStation()
							
							);
					SingleBike newEcoBike =new SingleBike(singlePane.getData().getId(),
							singlePane.getData().getName(),singlePane.getData().getType(),
							singlePane.getData().getWeight(),singlePane.getData().getLicensePlate(),
							singlePane.getData().getManuafacturingDate(),singlePane.getData().getProducer(),
							singlePane.getData().getCost(), singlePane.getData().getIdDockingStation());
					
					ISingleBikeApi singleBikeApi = SingleBikeApi.getSingleton();
					deleteEcoBike = singleBikeApi.deleteSingleBike(newEcoBike);
					singlePane.deleteData(newEcoBike);
					
					button.setVisible(false);
					button1.setVisible(false);
					
				}
			}
		});
		
	
	}
	
	
}

