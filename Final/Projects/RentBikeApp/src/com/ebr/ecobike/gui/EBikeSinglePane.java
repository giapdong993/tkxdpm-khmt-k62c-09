package com.ebr.ecobike.gui;

import javax.swing.JLabel;

import com.ebr.bean.EBike;
import com.ebr.bean.EcoBike;

public class EBikeSinglePane extends EcoBikeSinglePane{
	private JLabel labelBatteryPercentage;
	private JLabel labelLoadCycles;
	private JLabel labelUsageTime;
	
	
	public EBikeSinglePane() {
		super();
	}
	
	public EBikeSinglePane(EcoBike ecobike) {
		this();
		this.t = ecobike;
		
		displayData();
	}
	
	@Override
	public void buildControls() {
		super.buildControls();

		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelBatteryPercentage = new JLabel();
		add(labelBatteryPercentage, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelLoadCycles = new JLabel();
		add(labelLoadCycles, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelUsageTime = new JLabel();
		add(labelUsageTime, c);
		
	
	}
	
	@Override
	public void displayData() {
		super.displayData();
		
		if (t instanceof EBike) {
			EBike eBike = (EBike) t;
			
			labelBatteryPercentage.setText("Battery Percentage: " + eBike.getBatteryPercentage());
			labelLoadCycles.setText("LoadCycles: " + eBike.getLoadCycles());
			labelUsageTime.setText("Quantity: " + eBike.getUsageTime());
		}
	}
}
