package com.ebr.ecobike.singlebike;



import com.ebr.bean.EcoBike;
import com.ebr.ecobike.gui.BikeSinglePane;

@SuppressWarnings("serial")
public class SingleBikeSinglePane extends BikeSinglePane {
	
	public SingleBikeSinglePane() {
		super();
	}
	
	public SingleBikeSinglePane(EcoBike ecobike) {
		this();
		this.t = ecobike;

		displayData();
	}
}
