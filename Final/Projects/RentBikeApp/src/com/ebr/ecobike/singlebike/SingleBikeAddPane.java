package com.ebr.ecobike.singlebike;

import java.util.Map;

import com.ebr.ecobike.gui.BikeAddPane;

@SuppressWarnings("serial")
public class SingleBikeAddPane extends BikeAddPane{
	public SingleBikeAddPane() {
		super();
	} 
	
	@Override
	public void buildControls() {
		super.buildControls();
		
	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		return res;
	}
}
