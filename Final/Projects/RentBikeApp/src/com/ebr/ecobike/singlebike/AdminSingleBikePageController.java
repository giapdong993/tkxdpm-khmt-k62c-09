package com.ebr.ecobike.singlebike;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ebr.bean.EcoBike;
import com.ebr.bean.SingleBike;
import com.ebr.ecobike.controller.AdminEcoBikePageController;
import com.ebr.ecobike.gui.EcoBikeAddPane;
import com.ebr.ecobike.gui.EcoBikeSinglePane;
import com.oms.bean.Media;

import abstractdata.ADataSearchPane;

import com.ebr.api.*;
import com.ebr.api.ecobike.Api.EcoBikeApi;
import com.ebr.api.ecobike.Api.SingleBikeApi;
import com.ebr.api.ecobike.interfaceApi.IEcoBikeApi;
import com.ebr.api.ecobike.interfaceApi.ISingleBikeApi;



public class AdminSingleBikePageController extends AdminEcoBikePageController{
	
	
	
	public AdminSingleBikePageController() {
		super();
	}
	
	

	@Override
	public List<? extends EcoBike> search(Map<String, String> searchParams) {
		ISingleBikeApi singleBikeApi = SingleBikeApi.getSingleton();
		return singleBikeApi.getSingleBikes(searchParams);
	}
	
	@Override
	public EcoBikeSinglePane createSinglePane() {
		return new SingleBikeSinglePane();
	}
	@Override
	public EcoBikeAddPane createAddPane() {
		return new SingleBikeAddPane();//la book
	}

	
	
	
	@Override
	public List<? extends EcoBike> add(Map<String, String> addParams) {
		
		 String id = null;
		 String name = null;
		 String type = null;
		 int weight = 0;
		 String licensePlate= null;
		 Date manuafacturingDate = null;
		 String producer= null;
		 int cost = 0;
		 String idDockingStation = null;
		for (String t: addParams.keySet()){
            String key = t.toString();
            if (key.equals("id")) {
            	id = addParams.get(t).toString();
            }
            if (key.equals("name")) {
            	name = addParams.get(t).toString();
            }
            if (key.equals("type")) {
            	type = addParams.get(t).toString();
            }
            if (key.equals("weight")) {
            	weight = Integer.parseInt(addParams.get(t));
            }
            if (key.equals("licensePlate")) {
            	licensePlate = addParams.get(t).toString();
            }
            if (key.equals("manuafacturingDate")) {
            	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                String dateInString = addParams.get(t).toString();

                try {

                    manuafacturingDate = formatter.parse(dateInString);
                    
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (key.equals("producer")) {
            	producer = addParams.get(t).toString();
            }
            if (key.equals("cost")) {
            	cost =Integer.parseInt(addParams.get(t)) ;
            }
            if(key.equals("idDockingStation")) {
            	idDockingStation = addParams.get(t).toString();
            }
            String value = addParams.get(t).toString();  
            System.out.println(key + " " + value);
            
		}
		
		SingleBike addSingleBike = new SingleBike (id,name,type,weight,licensePlate,manuafacturingDate,producer,cost,idDockingStation);
		if (addSingleBike.getId() !=  null) {
			ISingleBikeApi singleBikeApi = SingleBikeApi.getSingleton();
			return singleBikeApi.addSingleBike(addSingleBike);
		}else {
			return null;
		}
		
	}



	@Override
	public ADataSearchPane createSearchPane() {
		// TODO Auto-generated method stub
		return null;
	}

}

