package admindockingstation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ebr.api.DockingStationApi;
import com.ebr.api.ecobike.Api.EcoBikeApi;
import com.ebr.bean.DockingStation;
import com.ebr.bean.EcoBike;
import com.ebr.bean.SingleBike;
import com.ebr.dockingstation.DSAddPane;
import com.ebr.dockingstation.DSSinglePane;
import com.ebr.ecobike.gui.AdminEcoBikeListPane;
import com.ebr.ecobike.gui.EcoBikeAddPane;
import com.ebr.ecobike.gui.EcoBikeSinglePane;
import com.ebr.ecobike.singlebike.SingleBikeSinglePane;

import abstractdata.ADataListPane;
import abstractdata.ADataPageController;
import abstractdata.ADataSearchPane;


public class AdminDSPageController extends ADataPageController<DockingStation> {
	
	public AdminDSPageController() {

	}

	@Override
	public ADataListPane<DockingStation> createListPane() {
		return new AdminDSListPane(this);
	}
	
	 public List<DockingStation> delete(String stationId, String stationName, String stationAddress, String type, 
			 int numberOfAllDocks, int numberOfBikes, int numberOfEBikes, int numberOfTwinBikes, ArrayList<String> id) {
		 DockingStation  ds = new DockingStation(stationId,stationName,stationAddress,type,numberOfAllDocks,numberOfBikes,numberOfEBikes,numberOfTwinBikes, id);
		 return new DockingStationApi().deleteDockingStation(ds);
	 }
	 
	 @Override
		public List<DockingStation> search(Map<String, String> searchParams) {
			return new DockingStationApi().getDockingStations(searchParams);
		}
		
		@Override
		public DSSinglePane createSinglePane() {
			return new DSSinglePane();
		}
		@Override
		public DSAddPane createAddPane() {
			return new DSAddPane();
		}

				
		@Override
		public List<DockingStation> add(Map<String, String> addParams) {
			
			 String stationId = null;
			 String stationName = null;
			 String stationAddress = null;
			 String type = "dockingstation";
			 int numberOfAllDocks = 0;
			 int numberOfBikes = 0;
			 int numberOfEBikes = 0;
			 int numberOfTwinBikes = 0;
			 ArrayList<String> id = null;
			 
			for (String t: addParams.keySet()){
	            String key = t.toString();
	            if (key.equals("stationId")) {
	            	stationId = addParams.get(t).toString();
	            }
	            if (key.equals("stationName")) {
	            	stationName = addParams.get(t).toString();
	            }
	            if (key.equals("stationAddress")) {
	            	stationAddress = addParams.get(t).toString();
	            }
	            if (key.equals("type")) {
	            	type = addParams.get(t).toString();
	            }
	            if (key.equals("numberOfAllDocks")) {
	            	numberOfAllDocks = Integer.parseInt(addParams.get(t));
	            }
	            if (key.equals("numberOfBikes")) {
	            		numberOfBikes = Integer.parseInt(addParams.get(t));
	            }
	            if (key.equals("numberOfEBikes")) {
	            	numberOfEBikes =Integer.parseInt(addParams.get(t)) ;
	            }
	            if (key.equals("numberOfTwinBikes")) {
	            	numberOfTwinBikes =Integer.parseInt(addParams.get(t)) ;
	            }
	            String value = addParams.get(t).toString();  
	            System.out.println(key + " " + value);
	            
			}
			
			DockingStation addDockingStation = new DockingStation (stationId,stationName,stationAddress,type,numberOfAllDocks,
					numberOfBikes,numberOfEBikes,numberOfTwinBikes,id);
			if (addDockingStation.getStationId() !=  null ) {
				return  new DockingStationApi().addDockingStation(addDockingStation);
			}else {
				return null;
			}
			
		}

		@Override
		public List<? extends EcoBike> delete(String id, String name, String type, int weight, String licensePlate,
				Date manuafacturingDate, String producer, int cost, String idDockingStation) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ADataSearchPane createSearchPane() {
			// TODO Auto-generated method stub
			return null;
		}
		
	
}
