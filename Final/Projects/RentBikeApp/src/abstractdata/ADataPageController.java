package abstractdata;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import com.ebr.bean.EcoBike;
import com.ebr.bean.SingleBike;
import com.ebr.ecobike.gui.EcoBikeAddPane;

public abstract class ADataPageController<T> {
	private ADataPagePane<T> pagePane;
	private ADataPagePane<T> pageAddPane;
	
	public ADataPageController() {
		ADataSearchPane searchPane = createSearchPane();
		
		ADataAddPane addPane = createAddPane();
		
		ADataListPane<T> listPane = createListPane();
		
		addPane.setController(new IDataAddController() {
			@Override
			public void add(Map<String, String> addParams) {
				List<? extends T> list = ADataPageController.this.search(null);
				listPane.updateData(list);
				List<? extends T> list1 = ADataPageController.this.add(addParams);
				if(list1 != null) {
					listPane.updateData(list1);
				}
				
			}
		});
		addPane.fireAddEvent();
		
	
		
		
		
		pagePane = new ADataPagePane<T>(addPane,listPane);
	}
	
	
	public JPanel getDataPagePane() {
		return pagePane;
	}
	
	
	public abstract ADataSearchPane createSearchPane();
	
	public abstract ADataAddPane createAddPane();
	
	       protected abstract List<? extends T> add(Map<String, String> addParams);


	public abstract List<? extends T> search(Map<String, String> searchParams);
	
	
	
	public abstract ADataSinglePane<T> createSinglePane();
	
	public abstract ADataListPane<T> createListPane();


	


	public abstract List<? extends EcoBike> delete(String id, String name, String type, int weight,
			String licensePlate, Date manuafacturingDate, String producer, int cost,String idDockingStation);


	
	
	
	}

