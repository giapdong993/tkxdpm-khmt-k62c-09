package abstractdata;

public interface IDataManageController<T> {
	
	public void onAct(T t);
}
