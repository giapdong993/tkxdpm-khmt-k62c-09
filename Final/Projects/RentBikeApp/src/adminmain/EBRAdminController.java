package adminmain;

import javax.swing.JPanel;

import com.ebr.ecobike.singlebike.AdminSingleBikePageController;
import com.ebr.ecobike.singleebike.AdminSingleEBikePageController;
import com.ebr.ecobike.twinbike.AdminTwinBikePageController;

import admindockingstation.AdminDSPageController;



public class EBRAdminController {	
	public EBRAdminController() {
		
	}
	
	public JPanel getSingleBikePage() {
		AdminSingleBikePageController controller = new AdminSingleBikePageController();
		
		return controller.getDataPagePane();
	}
	public JPanel getSingleEBikePage() {
		AdminSingleEBikePageController controller = new AdminSingleEBikePageController();
		
		return controller.getDataPagePane();
	}
	public JPanel getTwinBikePage() {
		AdminTwinBikePageController controller = new AdminTwinBikePageController();
		
		return controller.getDataPagePane();
	}
	
	public JPanel getDockingStationPage() {
		AdminDSPageController controller = new AdminDSPageController();
		
		return controller.getDataPagePane();
	}
}
